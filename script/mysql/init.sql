create database if not exists webook;
create database if not exists webook_intr;

-- auto-generated definition
create table if not exists retry
(
    id             bigint auto_increment
        primary key,
    `limit`        bigint           not null,
    type           longtext         not null,
    task_key       varchar(100)     not null,
    retry_request  longtext         not null,
    reason         longtext         null,
    ctime          bigint           not null,
    utime          bigint           not null,
    dtime          bigint default 0 not null,
    interval_milli bigint           not null,
    status         bigint default 1 not null,
    expire_time    bigint           not null comment '任务过期时间'
);

create index uk_retry
    on retry (task_key, dtime);

