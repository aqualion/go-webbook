package main

import (
	"gitee.com/aqualion/go-webbook/conf"
	"github.com/gin-gonic/gin"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"log/slog"
	"net/http"
	"strings"
)

var mode string
var cfile string
var cProvider string
var cEndPoint string
var cPath string

func init() {
	pflag.StringVarP(&mode, "mode", "m", "remote", "configuration mode: REMOTE/FILE")
	mode = strings.ToUpper(mode)
	pflag.StringVarP(&cfile, "config", "c",
		"config/config.yaml", "配置文件路径")
	pflag.StringVarP(&cProvider, "provider", "p", "etcd3", "viper remote provider")
	pflag.StringVarP(&cEndPoint, "endpoint", "e", "http://127.0.0.1:12379", "viper provider host")
	pflag.StringVarP(&cPath, "path", "h", "webook", "viper provider key")
	pflag.Parse()

}

func main() {
	slog.Info("配置模式为: " + mode)
	switch mode {
	case "REMOTE":
		slog.Info("从远程读取配置文件")
		conf.InitConfigurationEtcd(cProvider, cEndPoint, cPath)
	case "FILE":
		slog.Info("从指定文件读取配置文件")
		conf.InitConfigurationWatch(cfile)
	default:
		panic("unknown configuration mode: " + mode)
	}

	server := InitWebServer()
	initPrometheus()
	initOTEL()

	server.GET("/", func(context *gin.Context) {
		context.String(http.StatusOK, "Hello, this is webook")
	})

	server.Run(":" + viper.GetString("app.port"))
}
