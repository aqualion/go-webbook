package main

import (
	"gitee.com/aqualion/go-webbook/conf"
	"gitee.com/aqualion/go-webbook/pkg/otelx"
	"github.com/spf13/viper"
	"log/slog"
)

func initOTEL() {
	setting := conf.OTELSetting{}
	err := viper.UnmarshalKey("otel", &setting)
	if err != nil {
		panic(err)
	}
	_, confNotifier := otelx.InitOTEL(
		otelx.WithEnabled(setting.Enabled),
		otelx.WithServiceName(setting.ServiceName),
		otelx.WithServiceVersion(setting.ServiceVersion),
		otelx.WithTracerProviderURL(setting.TracerProviderURL),
		otelx.WithSettingFetcher(func() otelx.OTELSetting {
			s := otelx.OTELSetting{}
			err := viper.UnmarshalKey("otel", &s)
			if err != nil {
				slog.Error("修改配置文集发生错误", slog.Any("error", err))
			}
			return s
		}),
	)
	conf.RegisterNotifier(confNotifier)
	slog.Info("OTEL inited!")
}
