//go:build wireinject

package main

import (
	"gitee.com/aqualion/go-webbook/internal/ioc"
	"gitee.com/aqualion/go-webbook/internal/repository"
	articleRepo "gitee.com/aqualion/go-webbook/internal/repository/article"
	"gitee.com/aqualion/go-webbook/internal/repository/cache"
	"gitee.com/aqualion/go-webbook/internal/repository/dao"
	articleDao "gitee.com/aqualion/go-webbook/internal/repository/dao/article"
	"gitee.com/aqualion/go-webbook/internal/service"
	"gitee.com/aqualion/go-webbook/internal/web"
	ijwt "gitee.com/aqualion/go-webbook/internal/web/jwt"
	"github.com/gin-gonic/gin"
	"github.com/google/wire"
)

var statsSvcProvider = wire.NewSet(
	dao.NewGORMInteractiveStatsDAO,
	ioc.InitMessenger,
	ioc.InitInteractiveStatsRepo,
	ioc.InitStatsServiceConf,
	service.NewInteractiveStatsServiceImpl,
	web.NewInteractiveStatsHandler,
)

var articleSvcProvider = wire.NewSet(
	articleDao.NewGORMArticleDAO,
	articleDao.NewReaderDAO,
	articleDao.NewAuthorDAO,
	cache.NewRedisArticleCache,
	articleRepo.NewArticleRepository,
	service.NewArticleService,
	web.NewArticleHandler,
)

//var interactiveSvcProvider = wire.NewSet(
//	service.NewInteractiveService,
//	repository.NewCachedInteractiveRepository,
//	cache.NewRedisInteractiveCache,
//	dao.NewGORMInteractiveDAO,
//)

func InitWebServer() *gin.Engine {
	wire.Build(
		ioc.InitRedis, ioc.InitDB,
		ioc.InitLogger,

		statsSvcProvider,
		//interactiveSvcProvider,
		articleSvcProvider,

		// DAO 部分
		dao.NewGORMUserDAO,
		dao.NewGORMRetryDAO,

		// Cache 部分
		cache.NewRedisUserCache,
		ioc.InitCodeCache,
		//cache.NewRedisCodeCache,
		//cache.NewCodeCacheGoBestPractice,

		// repository 部分
		repository.NewCachedUserRepository,
		repository.NewCodeRepository,
		repository.NewRetryRepository,
		//repository.NewCachedCodeRepository,

		// service 部分
		//ioc.InitSmsService,
		ioc.InitWechatService,
		service.NewUserService,
		service.NewCodeService,
		ioc.InitSMSService,
		ioc.InitSMSServiceAsync,

		// handler 部分
		ijwt.NewRedisHandler,
		web.NewUserHandler,
		web.NewHealthHandler,
		web.NewOAuth2WechatHandler,

		// gin 的中间件
		ioc.GinMiddlewares,

		// Web 服务器
		ioc.InitWebServer,
	)
	// 随便返回一个
	return gin.Default()
}
