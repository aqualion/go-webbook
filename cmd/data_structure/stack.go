package data_structure

import "fmt"

type Stack struct {
	items []int
}

func NewStack() *Stack {
	return &Stack{}
}

// Push 入栈
func (s *Stack) Push(item int) {
	// 使用快速排序对数组进行排序
	s.items = append(s.items, item)
}

// Pop 弹出栈顶元素
func (s *Stack) Pop() (int, error) {
	if len(s.items) == 0 {
		return 0, fmt.Errorf("Popped from empty stack")
	}
	item := s.items[len(s.items)-1]
	s.items = s.items[:len(s.items)-1]
	return item, nil
}

// Peek 返回栈顶元素
func (s *Stack) Peek() (int, error) {
	if len(s.items) == 0 {
		return 0, fmt.Errorf("Stack is empty")
	}
	return s.items[len(s.items)-1], nil
}

// IsEmpty 判断栈是否为空
func (s *Stack) IsEmpty() bool {
	return len(s.items) == 0
}
