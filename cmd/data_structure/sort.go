package data_structure

// BubbleSort 冒泡排序
// 时间复杂度：O(n^2)
// 空间复杂度：O(1)
// 稳定性：稳定
// 思路: 从第一个元素开始，将其与后面的元素比较，如果大于后面的元素，则将其与后面的元素交换位置
func BubbleSort(arr []int) []int {
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr)-i-1; j++ {
			if arr[j] > arr[j+1] {
				arr[j], arr[j+1] = arr[j+1], arr[j]
			}
		}
	}
	return arr
}

// InsertionSort 插入排序
// 时间复杂度：O(n^2)
// 空间复杂度：O(1)
// 稳定性：稳定
// 思路: 从第二个元素开始，将其与前面的元素比较，如果小于前面的元素，则将其与前面的元素交换位置
func InsertionSort(arr []int) []int {
	for i := 1; i < len(arr); i++ {
		for j := i; j > 0 && arr[j] < arr[j-1]; j-- {
			arr[j], arr[j-1] = arr[j-1], arr[j]
		}
	}
	return arr
}

// SelectionSort 选择排序
// 时间复杂度：O(n^2)
// 空间复杂度：O(1)
// 稳定性：不稳定, 例子: [5,8,5,2,9]
// 思路: 每次找到最小的数，将其与第一个数交换位置
func SelectionSort(arr []int) []int {
	for i := 0; i < len(arr); i++ {
		min := i
		for j := i + 1; j < len(arr); j++ {
			if arr[j] < arr[min] {
				min = j
			}
		}
		arr[i], arr[min] = arr[min], arr[i]
	}
	return arr
}

// MergeSort 归并排序
// 时间复杂度：O(nlogn)
// 空间复杂度：O(n)
// 稳定性：稳定
// 思路: 将数组分为左右两部分，然后对左右两部分分别进行排序，最后将左右两部分合并为一个有序数组
func MergeSort(arr []int) []int {
	if len(arr) <= 1 {
		return arr
	}
	mid := len(arr) / 2
	left := MergeSort(arr[:mid])
	right := MergeSort(arr[mid:])
	return merge(left, right)
}

// merge 将两个有序数组合并为一个有序数组
func merge(left, right []int) []int {
	res := make([]int, len(left)+len(right))
	var (
		i, j, k int
	)

	for e := 0; e < len(res) && i < len(left) && j < len(right); e++ {
		leftVal := left[i]
		rightVal := right[j]
		if leftVal <= rightVal {
			res[k] = leftVal
			i++
			k++
		} else {
			res[k] = rightVal
			j++
			k++
		}
	}

	// 判断哪个子数组中还有数据，并将数据拷贝到结果数组中
	if i >= len(left) {
		for e := j; e < len(right); e++ {
			res[k] = right[e]
			k++
		}
	} else {
		for e := i; e < len(left); e++ {
			res[k] = left[e]
			k++
		}
	}

	return res
}

func QuickSort(arr []int) []int {
	return quickSort(arr, 0, len(arr)-1)
}

// FindTheKMaxNumber 找出数组中第k大的数
func FindTheKMaxNumber(arr []int, k int) (int, bool) {
	return findTheKMaxNumber(arr, 0, len(arr)-1, k)
}

// quickSort 快速排序
// 时间复杂度：O(nlogn)
// 空间复杂度：O(logn)
// 稳定性：不稳定
// 思路: 先选一个基准数，然后将比基准数小的数放在左边，比基准数大的数放在右边，然后对左右两边的数分别进行排序
func quickSort(arr []int, left, right int) []int {
	// 中断条件
	if left >= right {
		return arr
	}
	// 先原地寻找pivot
	i, j := left, right
	pivot := arr[left]
	for i < j {
		for i < j && arr[j] >= pivot {
			j--
		}
		arr[i] = arr[j]
		for i < j && arr[i] <= pivot {
			i++
		}
		arr[j] = arr[i]
	}
	arr[i] = pivot
	// 再递归排序
	quickSort(arr, left, i-1)
	quickSort(arr, i+1, right)
	return arr
}

func findTheKMaxNumber(arr []int, left, right, k int) (int, bool) {
	// 算不了
	if arr == nil || len(arr) < k || len(arr) == 0 {
		return 0, false
	}
	// 中断条件
	if left >= right {
		return arr[left], true
	}
	// 先原地寻找pivot
	i, j := left, right
	pivotValue := arr[left]
	for i < j {
		for i < j && arr[j] >= pivotValue {
			j--
		}
		arr[i] = arr[j]
		for i < j && arr[i] <= pivotValue {
			i++
		}
		arr[j] = arr[i]
	}
	arr[i] = pivotValue
	pivotIdx := i

	if pivotIdx+1 == k {
		return arr[pivotIdx], true
	} else if pivotIdx+1 > k {
		return findTheKMaxNumber(arr, left, i-1, k)
	} else {
		return findTheKMaxNumber(arr, i+1, right, k)
	}
}
