package data_structure

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// 测试SkipList
func TestSkipListTest(t *testing.T) {
	sl := NewSkipList()
	sl.Insert(123)
	sl.Insert(456)
	r1 := sl.Search(123)
	assert.Equal(t, 123, r1.value)

	r2 := sl.Search(789)
	assert.Nil(t, r2)
}
