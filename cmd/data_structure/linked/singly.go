package linked

type SingyLinkedList[T any] struct {
	head   *SinglyNode[T]
	tail   *SinglyNode[T]
	length int
}

type SinglyNode[T any] struct {
	value T
	next  *SinglyNode[T]
}

func NewSinglyLinkedList[T any]() *SingyLinkedList[T] {
	// 用哨兵节点简化代码逻辑
	sentinel := &SinglyNode[T]{}
	return &SingyLinkedList[T]{head: sentinel, tail: sentinel}
}

// GetNode returns the node at the given index
func (l *SingyLinkedList[T]) GetNode(idx int) *SinglyNode[T] {
	node := l.head
	for i := 0; i < idx; i++ {
		node = node.next
	}
	return node
}

// Insert inserts a new node at the given index
func (l *SingyLinkedList[T]) Insert(idx int, value T) {
	// 获取待插入位置的前一个节点
	prevNode := l.GetNode(idx)
	// 插入新节点
	newNode := &SinglyNode[T]{value: value, next: prevNode.next}
	prevNode.next = newNode
	// 更新尾节点
	if l.tail == prevNode {
		l.tail = newNode
	}
	l.length++
}

// Delete deletes the node at the given index
func (l *SingyLinkedList[T]) Delete(idx int) {
	// 获取待删除位置的前一个节点
	prevNode := l.GetNode(idx)
	// 删除节点
	prevNode.next = prevNode.next.next
	// 更新尾节点
	if prevNode.next == nil {
		l.tail = prevNode
	}
	l.length--
}

// Reverse1 使用: 就地逆置法, 时间复杂度O(n), 空间复杂度O(1)
func (l *SingyLinkedList[T]) Reverse1() {
	if l.head.next == nil {
		return
	}

	// 从第一个节点开始，每次操作两个节点a,b
	// 每次摘除b(摘除操作的实现就是: 把a的next指针指向b的next)
	// 然后将b移动到表头(head节点的next)
	// 整个过程如下:
	// 1(a)->2(b)->3->4
	// 2->1(a)->3(b)->4
	// 3->2->1(a)->4(b)
	// 4->3->2->1
	a := l.head.next
	for a.next != nil {
		b := a.next
		// 摘除b节点(a的next指针指向b的next)
		a.next = b.next
		// 将b指向原本的第1个元素
		b.next = l.head.next
		// 然后将b变成第1个元素
		l.head.next = b
	}
	// 反转完成后更新尾节点
	l.tail = a
}

// Reverse2 使用: 新建链表逆置法, 时间复杂度O(n), 空间复杂度O(n)
func (l *SingyLinkedList[T]) Reverse2() {
	if l.head.next == nil {
		return
	}

	// 新建一个链表, 用于存放逆置后的结果
	newList := NewSinglyLinkedList[T]()
	// 从头到尾遍历原链表, 将每个节点插入到新链表的头部
	for node := l.head.next; node != nil; node = node.next {
		newList.Insert(0, node.value)
	}
	// 将新链表赋值给原链表
	l.head = newList.head
	l.tail = newList.tail
}

// Reverse3 使用: 递归逆置法, 时间复杂度O(n), 空间复杂度O(n)
func (l *SingyLinkedList[T]) Reverse3() {
	if l.head.next == nil {
		return
	}
	// 递归逆置
	l.head.next = l.recurse(l.head.next)
	// 反转完成后更新尾节点
	l.tail = l.GetNode(l.Length() - 1)
}

// Length returns the length of the linked list
func (l *SingyLinkedList[T]) Length() int {
	return l.length
}

// recurse 递归逆置
func (l *SingyLinkedList[T]) recurse(node *SinglyNode[T]) *SinglyNode[T] {
	if node.next == nil {
		return node
	}
	// 递归逆置
	newHead := l.recurse(node.next)
	// 反转指针
	node.next.next = node
	node.next = nil
	return newHead
}
