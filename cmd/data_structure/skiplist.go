package data_structure

import "math/rand"

// 跳表节点
type SkipListNode struct {
	// 节点存储的值
	value int
	// 节点层级
	level int
	// 节点指针数组
	forwards []*SkipListNode
}

// SkipList 跳表结构体
type SkipList struct {
	// 跳表头节点
	head *SkipListNode
	// 跳表当前层级
	level int
	// 跳表最大层级
	maxLevel int
}

// NewSkipListNode 初始化跳表节点
func NewSkipListNode(v int, level int) *SkipListNode {
	return &SkipListNode{v, level, make([]*SkipListNode, level)}
}

// NewSkipList 初始化跳表
func NewSkipList() *SkipList {
	// 头节点存储的值为0, 头节点的层级为跳表最大层级
	return &SkipList{NewSkipListNode(0, 32), 1, 32}
}

// Search 查找跳表中是否存在某个值, 返回该值所在节点, 如果不存在则返回nil
func (sl *SkipList) Search(v int) *SkipListNode {
	// 从头节点开始查找
	cur := sl.head
	// 从最高层级开始查找
	for i := sl.level - 1; i >= 0; i-- {
		// 查找当前层级中小于查找值的节点
		for cur.forwards[i] != nil && cur.forwards[i].value < v {
			cur = cur.forwards[i]
		}
		// 如果当前层级中存在等于查找值的节点，则返回该节点
		if cur.forwards[i] != nil && cur.forwards[i].value == v {
			return cur.forwards[i]
		}
	}
	return nil
}

// Insert 插入节点到跳表中, 返回插入节点的层级
func (sl *SkipList) Insert(v int) int {
	// 获取插入节点的层级
	level := sl.randomLevel()
	// 初始化插入节点
	newNode := NewSkipListNode(v, level)
	// 从头节点开始查找
	cur := sl.head
	// 从最高层级开始查找
	for i := level - 1; i >= 0; i-- {
		// 查找当前层级中小于插入值的节点
		for cur.forwards[i] != nil && cur.forwards[i].value < v {
			cur = cur.forwards[i]
		}
		// 将插入节点的指针指向下一个节点
		newNode.forwards[i] = cur.forwards[i]
		// 将当前节点的指针指向插入节点
		cur.forwards[i] = newNode
	}
	// 如果插入节点的层级大于跳表的层级，则更新跳表的层级
	if level > sl.level {
		sl.level = level
	}
	return level

}

// randomLevel 随机生成节点层级
func (sl *SkipList) randomLevel() int {
	level := 1
	for i := 1; i < sl.maxLevel; i++ {
		if rand.Int31()%7 == 1 {
			level++
		}
	}
	return level
}
