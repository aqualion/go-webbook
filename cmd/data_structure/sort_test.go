package data_structure

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFindTheKMaxNumber(t *testing.T) {
	type args struct {
		arr []int
		k   int
	}
	tests := []struct {
		name  string
		args  args
		want  int
		want1 bool
	}{
		{
			name: "pivot就是第k大值",
			args: args{
				arr: []int{4, 2, 5, 12, 3},
				k:   3,
			},
			want:  4,
			want1: true,
		},
		{
			name: "第k大值在pivot左区间",
			args: args{
				arr: []int{5, 2, 4, 12, 3},
				k:   3,
			},
			want:  4,
			want1: true,
		},
		{
			name: "第k大值在pivot右区间",
			args: args{
				arr: []int{3, 2, 4, 12, 5},
				k:   3,
			},
			want:  4,
			want1: true,
		},
		{
			name: "空数组",
			args: args{
				arr: []int{},
				k:   3,
			},
			want:  0,
			want1: false,
		},
		{
			name: "nil数组",
			args: args{
				arr: nil,
				k:   3,
			},
			want:  0,
			want1: false,
		},
		{
			name: "k小于数组长度",
			args: args{
				arr: []int{1, 2},
				k:   3,
			},
			want:  0,
			want1: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := FindTheKMaxNumber(tt.args.arr, tt.args.k)
			assert.Equalf(t, tt.want, got, "FindTheKMaxNumber(%v, %v)", tt.args.arr, tt.args.k)
			assert.Equalf(t, tt.want1, got1, "FindTheKMaxNumber(%v, %v)", tt.args.arr, tt.args.k)
		})
	}
}

func TestQuickSort(t *testing.T) {
	type args struct {
		arr []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "双指针交换2",
			args: args{
				arr: []int{1, 7, 5, 6},
			},
			want: []int{1, 5, 6, 7},
		},
		{
			name: "双指针交换",
			args: args{
				arr: []int{5, 4, 3, 2, 1},
			},
			want: []int{1, 2, 3, 4, 5},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, QuickSort(tt.args.arr), "QuickSort(%v)", tt.args.arr)
		})
	}
}

func TestGolangSort(t *testing.T) {
}
