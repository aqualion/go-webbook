package data_structure

func recurse(n int) int {
	if n == 1 {
		return 1
	}
	return recurse(n-1) + 1
}
