package data_structure

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_recurse(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "第一排",
			args: args{
				n: 1,
			},
			want: 1,
		},

		{
			name: "第15排",
			args: args{
				n: 15,
			},
			want: 15,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, recurse(tt.args.n), "recurse(%v)", tt.args.n)
		})
	}
}
