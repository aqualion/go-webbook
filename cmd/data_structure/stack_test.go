package data_structure

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestStack(t *testing.T) {
	// 测试栈
	stack := NewStack()
	stack.Push(1)
	stack.Push(2)
	stack.Push(3)

	t.Log(stack.Pop())
	t.Log(stack.Pop())
	t.Log(stack.Pop())
	t.Log(stack.Pop())
	t.Log(stack.Pop())
	assert.True(t, stack.IsEmpty())
}
