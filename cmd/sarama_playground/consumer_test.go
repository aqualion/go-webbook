package sarama_playground

import (
	"context"
	"github.com/IBM/sarama"
	"github.com/stretchr/testify/require"
	"golang.org/x/sync/errgroup"
	"log"
	"sync"
	"testing"
	"time"
)

func TestConsumer(t *testing.T) {
	cfg := sarama.NewConfig()
	cfg.Consumer.Offsets.Initial = sarama.OffsetOldest
	// 正常来说，一个消费者都是归属于一个消费者的组的
	// 消费者组就是你的业务
	consumerGroup := "my_consumer_group"
	consumer, err := sarama.NewConsumerGroup(addrs,
		consumerGroup, cfg)
	require.NoError(t, err)

	// 带超时的 context
	start := time.Now()
	//ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	//defer cancel()

	//ctx, cancel := context.WithCancel(context.Background())
	//time.AfterFunc(time.Minute*10, func() {
	//	cancel()
	//})

	ctx := context.Background()

	err = consumer.Consume(ctx,
		[]string{topic}, testConsumerGroupHandler{})
	// 你消费结束，就会到这里
	t.Log(err, time.Since(start).String())
}

type testConsumerGroupHandler struct {
}

func (t testConsumerGroupHandler) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	return t.consumeClaimBatch(session, claim)
	//return t.consumeClaimBatchDaming(session, claim)
	//return t.consumeClaimNormal(session, claim)
}

func (t testConsumerGroupHandler) Setup(session sarama.ConsumerGroupSession) error {
	// topic => 偏移量
	partitions := session.Claims()[topic]

	for _, part := range partitions {
		log.Println("partition info:", part)
	}

	return nil
}

func (t testConsumerGroupHandler) Cleanup(session sarama.ConsumerGroupSession) error {
	log.Println("CleanUp")
	return nil
}

// 批量异步消费消息: 这里接到消息后，会异步消费，然后批量提交消息，这样可以减轻与kafka之间的网络IO
func (t testConsumerGroupHandler) consumeClaimBatch(
	// 代表的是你和Kafka 的会话（从建立连接到连接彻底断掉的那一段时间）
	session sarama.ConsumerGroupSession,
	claim sarama.ConsumerGroupClaim) error {

	msgs := claim.Messages()

	// consumingWindow(消费窗口时间)与batchSize(最大批次消费数)
	// 意思是：我这个consumer只消费指定时间段内接到的所有消息,且最多batchSize条
	// 例：如果3秒到了，我就commit所有目前异步消费完的msg
	// 例：如果3秒内已经消费了10条消息，我就commit所有目前异步消费完的msg
	consumingWindow := 3 * time.Second
	const batchSize = 10

	log.Println("target batch size:", batchSize)
	for {
		ctx, cancel := context.WithTimeout(context.Background(), consumingWindow)
		var eg errgroup.Group
		var last *sarama.ConsumerMessage
		var mu sync.Mutex
		var cnt int
	Outer:
		for i := 0; i < batchSize; i++ {
			select {
			case <-ctx.Done():
				// 这边代表超时了
				break Outer
			case msg, ok := <-msgs:
				if !ok {
					log.Println("msgs被关了")
					cancel()
					// 代表消费者被关闭了
					return nil
				}
				last = msg
				log.Println("收到消息, 开始异步消费")
				eg.Go(func() error {
					// 在这里消费和重试
					time.Sleep(time.Second)
					log.Println("假装在消费:", string(msg.Value))
					mu.Lock()
					cnt++
					mu.Unlock()
					return nil
				})
			}
		}
		cancel()
		err := eg.Wait()
		if err != nil {
			log.Println("异步消费错误: error group返回第一个错误:", err)
			// 这边能怎么办？
			// 记录日志 + 存入数据库，做补偿机制
			continue
		}
		if last != nil {
			log.Println("异步消费完成, commit最后一条msg")
			log.Println("本次总计完成消费数:", cnt)
			session.MarkMessage(last, "")
		}
	}
}

// 正常同步消费消息
func (t testConsumerGroupHandler) consumeClaimNormal(
	// 代表的是你和Kafka 的会话（从建立连接到连接彻底断掉的那一段时间）
	session sarama.ConsumerGroupSession,
	claim sarama.ConsumerGroupClaim) error {
	msgs := claim.Messages()
	log.Println("消费者会话已建立")
	for msg := range msgs {
		//var bizMsg MyBizMsg
		//err := json.Unmarshal(msg.Value, &bizMsg)
		//if err != nil {
		//	// 这就是消费消息出错
		//	// 大多数时候就是重试
		//	// 记录日志
		//	continue
		//}
		log.Println(string(msg.Value))
		session.MarkMessage(msg, "")
	}
	// 什么情况下会到这里
	// msgs 被人关了，也就是要退出消费逻辑
	return nil
}

func (t testConsumerGroupHandler) consumeClaimBatchDaming(
	// 代表的是你和Kafka 的会话（从建立连接到连接彻底断掉的那一段时间）
	session sarama.ConsumerGroupSession,
	claim sarama.ConsumerGroupClaim) error {
	msgs := claim.Messages()
	//for msg := range msgs {
	//	m1 := msg
	//	go func() {
	//		// 消费msg
	//		log.Println(string(m1.Value))
	//		session.MarkMessage(m1, "")
	//	}()
	//}
	const batchSize = 10
	for {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		var eg errgroup.Group
		var last *sarama.ConsumerMessage
		done := false
		for i := 0; i < batchSize && !done; i++ {
			select {
			case <-ctx.Done():
				log.Println("done!")
				done = true
			case msg, ok := <-msgs:
				if !ok {
					cancel()
					// 代表消费者被关闭了
					return nil
				}
				last = msg
				eg.Go(func() error {
					// 我就在这里消费
					time.Sleep(time.Second)
					// 你在这里重试
					log.Println("开始消费 offset:", msg.Offset)
					return nil
				})
			}
		}
		log.Println("消费窗口结束")
		cancel()
		err := eg.Wait()
		if err != nil {
			// 这边能怎么办？
			// 记录日志
			continue
		}
		// 就这样
		if last != nil {
			log.Println("commit")
			session.MarkMessage(last, "")
		}
	}
	//
	//label1:
	//	println("hellp")
}

type MyBizMsg struct {
	Name string
}

// 返回只读的 channel
func ChannelV1() <-chan struct{} {
	panic("implement me")
}

// 返回可读可写 channel
func ChannelV2() chan struct{} {
	panic("implement me")
}

// 返回只写 channel
func ChannelV3() chan<- struct{} {
	panic("implement me")
}
