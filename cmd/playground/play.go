package main

import (
	"log/slog"
	"os"
)

func main() {
	logger := slog.New(slog.NewJSONHandler(os.Stderr, nil))
	logger.Info("hello world", slog.Int("a", 1), slog.String("b", "b"))

}
