package main

import (
	"errors"
	"fmt"
)

func main() {
	// 删除切片元素
	beforeDelete := []int{1, 2, 3, 4, 5}
	fmt.Println("删除元素前的切片:", beforeDelete)
	afterDelete, err := DeleteAt(beforeDelete, 2)
	if err != nil {
		panic(err)
	}
	fmt.Println("删除元素后的切片:", afterDelete)

	// 缩容
	beforeShrink := make([]int, 1, 128)
	fmt.Printf("缩容前的容量: %v, 长度: %v\n", cap(beforeShrink), len(beforeShrink))
	afterShrink := Shrink(beforeShrink)
	fmt.Printf("缩容后的容量: %v, 长度: %v\n", cap(afterShrink), len(afterShrink))
}

var ErrIndexOutOfRange = errors.New("下标越界")

// DeleteAt 可以删除一个切片内指定index上的元素
func DeleteAt[T any](src []T, index int) ([]T, error) {
	length := len(src)
	if index < 0 || index >= length {
		return nil, fmt.Errorf("%w, 下标越界，长度%v, 下标%v", ErrIndexOutOfRange, length, index)
	}

	// 1,2,3,4,5 -> 1,2,4,5,5
	for i := index; i+1 < length; i++ {
		src[i] = src[i+1]
	}
	// 1,2,4,5,5 -> 1,2,4,5
	return src[:length-1], nil
}

// Shrink 可以将输入的切片进行缩容
func Shrink[T any](src []T) []T {
	c, l := cap(src), len(src)
	newCapacity, changed := calCapacity(c, l)
	if !changed {
		return src
	}
	s := make([]T, 0, newCapacity)
	s = append(s, src...)
	return s
}

// calCapacity 根据输入的长度和容量计算缩容后的新容量值
// 容量<=64时无需缩容
// 元素 <= 容量的一半时, 将容量变为当前容量的 0.625倍，与正向扩容的1.25倍相呼应
// 元素 <= 容量的四分之一, 将容量变为当前容量的 0.5倍
func calCapacity(c int, l int) (int, bool) {
	if c <= 64 {
		return c, false
	}
	cond := c / l
	switch {
	case cond >= 4:
		return c / 2, true
	case cond >= 2:
		factor := 0.625
		return int(float32(c) * float32(factor)), true
	default:
		return c, false
	}
}
