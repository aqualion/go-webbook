//go:build wireinject

package startup

import (
	"gitee.com/aqualion/go-webbook/internal/ioc"
	"gitee.com/aqualion/go-webbook/internal/repository"
	articleRepo "gitee.com/aqualion/go-webbook/internal/repository/article"
	"gitee.com/aqualion/go-webbook/internal/repository/cache"
	"gitee.com/aqualion/go-webbook/internal/repository/dao"
	"gitee.com/aqualion/go-webbook/internal/repository/dao/article"
	"gitee.com/aqualion/go-webbook/internal/service"
	"gitee.com/aqualion/go-webbook/internal/web"
	ijwt "gitee.com/aqualion/go-webbook/internal/web/jwt"
	"github.com/gin-gonic/gin"
	"github.com/google/wire"
)

var thirdProvider = wire.NewSet(InitRedis, InitTestDB, InitLog)
var userSvcProvider = wire.NewSet(
	dao.NewGORMUserDAO,
	cache.NewRedisUserCache,
	repository.NewCachedUserRepository,
	service.NewUserService)

var articlSvcProvider = wire.NewSet(
	article.NewGORMArticleDAO,
	articleRepo.NewArticleRepository,
	service.NewArticleService,
	article.NewReaderDAO,
	article.NewAuthorDAO,
	cache.NewRedisArticleCache,
)

var interactiveSvcProvider = wire.NewSet(
	service.NewInteractiveService,
	repository.NewCachedInteractiveRepository,
	dao.NewGORMInteractiveDAO,
	cache.NewRedisInteractiveCache,
)

var statsSvcProvider = wire.NewSet(
	dao.NewGORMInteractiveStatsDAO,
	ioc.InitMessenger,
	ioc.InitInteractiveStatsRepo,
	ioc.InitStatsServiceConf,
	service.NewInteractiveStatsServiceImpl,
	web.NewInteractiveStatsHandler,
)

func InitWebServer() *gin.Engine {
	wire.Build(
		thirdProvider,
		userSvcProvider,
		articlSvcProvider,
		statsSvcProvider,
		cache.NewRedisCodeCache,
		repository.NewCodeRepository,
		// service 部分
		// 集成测试我们显式指定使用内存实现
		ioc.InitSMSServiceInMemory,

		// 指定啥也不干的 wechat service
		InitPhantomWechatService,
		service.NewCodeService,
		// handler 部分
		web.NewUserHandler,
		web.NewOAuth2WechatHandler,
		web.NewArticleHandler,
		web.NewHealthHandler,
		ijwt.NewRedisHandler,

		// gin 的中间件
		ioc.GinMiddlewares,

		// Web 服务器
		ioc.InitWebServer,
	)
	// 随便返回一个
	return gin.Default()
}

func InitArticleHandler(dao article.ArticleDAO) *web.ArticleHandler {
	wire.Build(thirdProvider,
		userSvcProvider,
		//cache.NewRedisArticleCache,
		//wire.InterfaceValue(new(article.ArticleDAO), dao),
		article.NewReaderDAO,
		article.NewAuthorDAO,
		cache.NewRedisArticleCache,
		articleRepo.NewArticleRepository,
		service.NewArticleService,
		web.NewArticleHandler)
	return new(web.ArticleHandler)
}

func InitUserSvc() service.UserService {
	wire.Build(thirdProvider, userSvcProvider)
	return service.NewUserService(nil)
}

func InitJwtHdl() ijwt.Handler {
	wire.Build(thirdProvider, ijwt.NewRedisHandler)
	return ijwt.NewRedisHandler(nil)
}

func InitInteractiveService() service.InteractiveService {
	wire.Build(thirdProvider, interactiveSvcProvider)
	return service.NewInteractiveService(nil, nil)
}
