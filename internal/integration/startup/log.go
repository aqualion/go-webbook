package startup

import (
	"gitee.com/aqualion/go-webbook/pkg/logger"
	"go.uber.org/zap"
)

func InitLog() logger.LoggerV1 {
	cfg := zap.NewDevelopmentConfig()
	build, err := cfg.Build()
	if err != nil {
		panic(err)
	}
	return logger.NewZapLogger(build)
}
