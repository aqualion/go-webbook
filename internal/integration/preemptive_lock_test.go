package integration

import (
	"context"
	"fmt"
	"gitee.com/aqualion/go-webbook/internal/integration/startup"
	preemptive2 "gitee.com/aqualion/go-webbook/pkg/jobx/preemptive"
	"gitee.com/aqualion/go-webbook/pkg/logger"
	rlock "github.com/gotomicro/redis-lock"
	"github.com/stretchr/testify/assert"
	"math/rand"
	"testing"
	"time"
)

type tcInput struct {
	job         preemptive2.Job
	lock        *rlock.Client
	l           logger.LoggerV1
	lockTimeout time.Duration
}

// TestSmartJobWithRandomLoad 模拟三个节点在其负载不断变化的情况下，抢锁的运行情况
func TestSmartJobWithRandomLoad(t *testing.T) {
	t.Log("模拟三个节点在其负载不断变化的情况下，抢锁的运行情况")
	l := startup.InitLog()
	redisCmd := startup.InitRedis()
	var schedulers []*preemptive2.SmartScheduler
	for i := 1; i <= 3; i++ {
		scheduler := preemptive2.NewSmartJob(
			fmt.Sprintf("smart-%d", i),
			preemptive2.NewRedisPreemptiveJob(
				new(testJob),
				rlock.NewClient(redisCmd),
				l,
				10*time.Second),
			l)
		schedulers = append(schedulers, scheduler)
	}

	// 模拟负载变化
	go func() {
		for {
			for _, scheduler := range schedulers {
				scheduler.UpdateLoad(float64(rand.Intn(1000)))
			}
			time.Sleep(1 * time.Second)
		}
	}()

	// 模拟持续运行
	time.Sleep(1 * time.Second)
	for {
		fmt.Println()
		for _, scheduler := range schedulers {
			go scheduler.Run()
		}
		time.Sleep(10 * time.Second)
	}
}

// TestSmartJob
// 1. 模拟节点1持有锁后，因为负载在一段时间没持续很高而释放锁"
// 2. 模拟两个节点(负载一高一低)同时抢锁，负载低的应该先抢到
func TestSmartJob(t *testing.T) {
	l := startup.InitLog()
	redisCmd := startup.InitRedis()
	smart1 := preemptive2.NewSmartJob(
		"smart1",
		preemptive2.NewRedisPreemptiveJob(
			new(testJob),
			rlock.NewClient(redisCmd),
			l,
			20*time.Second),
		l)

	smart2 := preemptive2.NewSmartJob(
		"smart2",
		preemptive2.NewRedisPreemptiveJob(
			new(testJob),
			rlock.NewClient(redisCmd),
			l,
			20*time.Second),
		l)

	t.Log("模拟节点1持有锁后，因为负载在一段时间没持续很高而释放锁")
	err, lockHeld := smart1.Run()
	assert.NoError(t, err)
	assert.Equal(t, true, lockHeld)

	// 此时模拟load升高，导致锁释放
	smart1.UpdateLoad(600)
	// 另一个节点也模拟一个load， 但是比上面的低
	smart2.UpdateLoad(200)
	time.Sleep(20 * time.Second)

	// 此时锁应该是被释放了, 现在两个job同时抢锁，应该是smartJob2抢到锁 (因为smartJob1有负载，抢锁会有延迟)
	t.Log("模拟两个节点(负载一高一低)同时抢锁，负载低的应该先抢到")
	go func() {
		err, lockHeld := smart2.Run()
		assert.NoError(t, err)
		assert.Equal(t, true, lockHeld)
	}()
	err, lockHeld = smart1.Run()
	assert.NoError(t, err)
	assert.Equal(t, false, lockHeld)

	time.Sleep(5 * time.Second)
}

func TestPreemptiveLock(t *testing.T) {
	type testCase struct {
		input tcInput
	}

	tcs := []testCase{
		{
			input: tcInput{
				job:         new(testJob),
				lock:        rlock.NewClient(startup.InitRedis()),
				l:           startup.InitLog(),
				lockTimeout: 20 * time.Second,
			},
		},
	}

	for _, tc := range tcs {
		job := preemptive2.NewRedisPreemptiveJob(
			tc.input.job,
			tc.input.lock,
			tc.input.l,
			tc.input.lockTimeout)

		// 第一次执行，可以抢到锁并开始自动续约
		err, lockHeld := job.Run()
		assert.NoError(t, err)
		assert.Equal(t, true, lockHeld)

		// 第二次执行，由于有续约机制，所以锁应该还是有效的，就可以继续执行
		time.Sleep(5 * time.Second)
		err, lockHeld = job.Run()
		assert.Equal(t, true, lockHeld)
		assert.NoError(t, err)

		// 释放锁
		job.Close()

	}
}

type testJob struct{}

func (t *testJob) Name() string {
	return "this-is-a-test-job"
}

func (t *testJob) Run(ctx context.Context) error {
	time.Sleep(3 * time.Second)
	return nil
}
