package integration

import (
	"fmt"
	"gitee.com/aqualion/go-webbook/internal/integration/startup"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/suite"
	"golang.org/x/net/context"
	"gorm.io/gorm"
	"math/rand"
	"runtime"
	"sync"
	"testing"
	"time"
)

type InteractiveFakeDataSuite struct {
	suite.Suite
	db  *gorm.DB
	rdb redis.Cmdable
}

func TestInteractiveFakeData(t *testing.T) {
	suite.Run(t, &InteractiveFakeDataSuite{})
}

func (s *InteractiveFakeDataSuite) SetupSuite() {
	s.db = startup.InitTestDB()
	s.rdb = startup.InitRedis()
}

func (s *InteractiveFakeDataSuite) TestFakeData() {
	numCpu := runtime.NumCPU()
	fmt.Println("number of cpus", numCpu)
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	var wg sync.WaitGroup

	svc := startup.InitInteractiveService()
	startPos := 1
	totalLikes := 5000000
	workerNum := parseTaskPoolNum(numCpu)
	taskChan := make(chan int, workerNum)
	wg.Add(workerNum)

	go func() {
		for i := startPos; i <= totalLikes; i++ {
			randomBizId := r1.Intn(totalLikes) + 1
			taskChan <- randomBizId
		}
		close(taskChan)
	}()

	worker := func() {
		defer wg.Done()
		for {
			select {
			case bizId, ok := <-taskChan:
				if !ok {
					return
				}
				err := svc.Like(context.Background(), "article", int64(bizId), 1)
				if err != nil {
					s.T().Fatal(err)
				}
			default:
			}
		}
	}
	for i := 0; i < workerNum; i++ {
		go worker()
	}
	wg.Wait()
	fmt.Println("finished")

}

func parseTaskPoolNum(cpu int) int {
	if cpu-1 < 1 {
		return 1
	}
	return cpu - 1
}
