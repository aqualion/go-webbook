package integration

import (
	"context"
	_ "embed"
	"fmt"
	"gitee.com/aqualion/go-webbook/internal/domain"
	"gitee.com/aqualion/go-webbook/internal/integration/startup"
	"gitee.com/aqualion/go-webbook/internal/repository/cache"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLuaStatsRemakeZset(t *testing.T) {
	redis := startup.InitRedis()
	keyPrefix := "test"
	cacherName := "test-cacher"
	biz := "article"
	cntType := domain.CntTypeLike
	dynamicCacher := cache.NewRedisZsetCacher(redis, cache.NewCacherCommonImpl(keyPrefix, cacherName))

	data := []domain.InteractiveStats{
		{
			Biz:     biz,
			BizId:   3,
			Cnt:     30,
			CntType: cntType,
		},
		{
			Biz:     biz,
			BizId:   2,
			Cnt:     20,
			CntType: cntType,
		},
		{
			Biz:     biz,
			BizId:   1,
			Cnt:     10,
			CntType: cntType,
		},
	}
	err := dynamicCacher.SaveRank(context.Background(), biz, cntType, data)
	assert.NoError(t, err)

	for _, datum := range data {
		err := dynamicCacher.IncrRankIfPresent(context.Background(), biz, cntType, datum.BizId, 1)
		assert.NoError(t, err)
	}

	ranks, err := dynamicCacher.GetRank(context.Background(), biz, cntType, 1000)
	assert.NoError(t, err)
	for i, rank := range ranks {
		assert.Equal(t, rank.BizId, data[i].BizId)
		assert.Equal(t, rank.Cnt, data[i].Cnt+1)
	}
	err = redis.Del(context.Background(), fmt.Sprintf("stats:%s:%s:%s", keyPrefix, biz, cntType)).Err()
	assert.NoError(t, err)
}
