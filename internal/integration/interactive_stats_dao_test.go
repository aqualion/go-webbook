package integration

import (
	"context"
	"gitee.com/aqualion/go-webbook/internal/domain"
	"gitee.com/aqualion/go-webbook/internal/integration/startup"
	"gitee.com/aqualion/go-webbook/internal/repository/dao"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCountAndGroupByBiz(t *testing.T) {
	db := startup.InitTestDB()
	d := dao.NewGORMInteractiveStatsDAO(db)
	biz := "article"
	cntType := domain.CntTypeLike
	limit := 1000
	result, err := d.CountAndGroupByBiz(context.Background(), biz, cntType, limit)
	assert.NoError(t, err)
	assert.True(t, len(result) > 0)

	found, err := d.SelectByBiz(context.Background(), biz, cntType, limit)
	assert.NoError(t, err)
	assert.True(t, len(found) > 0)
}
