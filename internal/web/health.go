package web

import (
	"errors"
	"gitee.com/aqualion/go-webbook/pkg/ginx/wrappers"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

type HealthHandler struct{}

func NewHealthHandler() *HealthHandler {
	return &HealthHandler{}
}

func (h *HealthHandler) RegisterRoutes(server *gin.Engine) {
	g := server.Group("/health")
	g.GET("", h.Ping)
	g.POST("/test", wrappers.WrapReq(h.Test))
}

func (h *HealthHandler) Ping(ctx *gin.Context) {
	ctx.String(http.StatusOK, "pong")
}

type TestReq struct {
	Foo string
}

func (h *HealthHandler) Test(ctx *gin.Context, req TestReq) (wrappers.Result, error) {
	if strings.ToLower(req.Foo) != "foo" {
		return wrappers.Result{}, errors.New("不是foo!")
	}
	return wrappers.Result{
		Code: 200,
		Msg:  "success",
		Data: req.Foo,
	}, nil
}
