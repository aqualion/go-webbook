package web

import (
	"context"
	"gitee.com/aqualion/go-webbook/internal/service"
	"gitee.com/aqualion/go-webbook/pkg/ginx/wrappers"
	"gitee.com/aqualion/go-webbook/pkg/logger"
	"github.com/gin-gonic/gin"
)

type InteractiveStatsHandler struct {
	svc service.InteractiveStatsService
	l   logger.LoggerV1
}

func NewInteractiveStatsHandler(svc service.InteractiveStatsService, l logger.LoggerV1) *InteractiveStatsHandler {
	return &InteractiveStatsHandler{svc: svc, l: l}
}

func (h *InteractiveStatsHandler) RegisterRoutes(server *gin.Engine) {
	g := server.Group("/stats")
	g.POST("/ranks", wrappers.WrapBodyV1[GetRanksReq](h.GetRanks))
}

func (h *InteractiveStatsHandler) GetRanks(ctx *gin.Context, req GetRanksReq) (wrappers.Result, error) {
	result, err := h.svc.GetRank(context.Background(), req.Biz, req.CntType, req.Limit)
	if err != nil {
		return wrappers.Result{}, err
	}
	return wrappers.Result{
		Code: 200,
		Msg:  "success",
		Data: result,
	}, nil

}

type GetRanksReq struct {
	Biz     string `json:"biz"`
	CntType string `json:"cntType"`
	Limit   int    `json:"limit"`
}
