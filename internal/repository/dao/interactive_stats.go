package dao

import (
	"context"
	"fmt"
	"gitee.com/aqualion/go-webbook/internal/domain"
	"gorm.io/gorm"
	"time"
)

type InteractiveStatsDAO interface {
	CountAndGroupByBiz(ctx context.Context, biz, cntType string, limit int) ([]*InteractiveStatsModel, error)
	SelectByBiz(ctx context.Context, biz, cntType string, limit int) ([]*InteractiveStatsModel, error)
}

// InteractiveStatsModel 交互聚合统计
type InteractiveStatsModel struct {
	Id      int64  `gorm:"primaryKey,autoIncrement"`
	BizId   int64  `gorm:"uniqueIndex:uk_bizid_biz"`
	Biz     string `gorm:"type:varchar(128);uniqueIndex:uk_bizid_biz"`
	CntType string `gorm:"type:varchar(32)"`
	Cnt     int64

	Ctime int64
	Utime int64
}

func (i InteractiveStatsModel) TableName() string {
	return "interactive_stats"
}

type StatsEntry struct {
	Biz   string
	BizId int64
	Cnt   int64
}

type GORMInteractiveStatsDAO struct {
	db *gorm.DB
}

func NewGORMInteractiveStatsDAO(db *gorm.DB) InteractiveStatsDAO {
	return &GORMInteractiveStatsDAO{db: db}
}

// CountAndGroupByBiz 从interactive表中统计出信息后，再插入到interactive_stats表
func (d *GORMInteractiveStatsDAO) CountAndGroupByBiz(ctx context.Context, biz, cntType string, limit int) ([]*InteractiveStatsModel, error) {
	cntField := parseCntType(cntType)
	// SELECT biz, biz_id, %s as cnt FROM interactives WHERE biz = ? ORDER BY %s DESC LIMIT %d;
	//	sql := fmt.Sprintf(`SELECT biz, biz_id, SUM(%s) as cnt
	//FROM interactives WHERE biz = ? GROUP BY biz, biz_id ORDER BY cnt DESC LIMIT %d`, cntField, limit)
	sql := fmt.Sprintf(`SELECT biz, biz_id, %s as cnt FROM interactives WHERE biz = ? ORDER BY %s DESC LIMIT %d`, cntField, cntField, limit)
	var entries []StatsEntry
	err := d.db.WithContext(ctx).Raw(sql, biz).Scan(&entries).Error
	if err != nil {
		return nil, err
	}
	if len(entries) < 1 {
		return []*InteractiveStatsModel{}, nil
	}

	now := time.Now().UnixMilli()
	entities := make([]*InteractiveStatsModel, 0, len(entries))
	for _, entry := range entries {
		entities = append(entities, &InteractiveStatsModel{
			BizId:   entry.BizId,
			Biz:     biz,
			Cnt:     entry.Cnt,
			CntType: cntType,
			Ctime:   now,
			Utime:   now,
		})
	}

	err = d.db.WithContext(ctx).Transaction(func(tx *gorm.DB) error {
		if err := tx.Model(InteractiveStatsModel{}).
			Where("biz = ? and cnt_type = ?", biz, cntType).
			Delete(nil).Error; err != nil {
			return err
		}
		if err := tx.Model(InteractiveStatsModel{}).Save(entities).Error; err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return entities, nil
}

// SelectByBiz 从interactive_stats表中读取数据
func (d *GORMInteractiveStatsDAO) SelectByBiz(ctx context.Context, biz, cntType string, limit int) ([]*InteractiveStatsModel, error) {
	var found []*InteractiveStatsModel

	err := d.db.WithContext(ctx).
		Where("biz = ? and cnt_type = ?", biz, cntType).
		Limit(limit).
		Find(&found).Error

	if err != nil {
		return nil, err
	}
	return found, nil
}

func parseCntType(cntType string) string {
	switch cntType {
	case domain.CntTypeLike:
		return "like_cnt"
	case domain.CntTypeRead:
		return "read_cnt"
	case domain.CntTypeCollection:
		return "collect_cnt"
	default:
		return ""
	}
}
