package dao

import (
	"context"
	"gitee.com/aqualion/go-webbook/internal/domain"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"time"
)

var ErrRetryDuplicate = errors.New("重试条目重复")

type RetryDAO interface {
	Insert(ctx context.Context, model Retry) error
	SelectByTaskKey(ctx context.Context, key string) (*Retry, error)
	UpdateByTaskKeyAndOptionalDelete(ctx context.Context, taskKey string, entry domain.Retry, delete bool) error
	BatchUpdateByTaskKeyAndOptionalDelete(ctx context.Context, taskKey []string, entry domain.Retry, delete bool) error
	ListValid(ctx context.Context) ([]*Retry, error)
}

type Retry struct {
	Id            int64  `gorm:"primaryKey,autoIncrement"`
	Limit         int    // 最大重试数, 超过后放弃重试
	Type          string // 标志是什么类型的重试任务
	TaskKey       string // task的唯一标志符
	RetryRequest  string // json字符串，记录重试请求
	Reason        string // 触发重试原因
	Ctime         int64
	Utime         int64
	Dtime         int64 // 删除时间，默认为0
	IntervalMilli int64 // 重试间隔时间(毫秒)
	ExpireTime    int64 // 过期时间
	Status        int   // 0: 重试失败, 1: 重试成功
}

func (r Retry) TableName() string {
	return "retry"
}

type GORMRetryDAO struct {
	db *gorm.DB
}

func (d *GORMRetryDAO) ListValid(ctx context.Context) ([]*Retry, error) {
	var list []*Retry
	err := d.db.WithContext(ctx).Find(&list, "dtime = 0 and status = ?", domain.StatusRetrying).Error
	return list, err
}

func (d *GORMRetryDAO) BatchUpdateByTaskKeyAndOptionalDelete(ctx context.Context, taskKeys []string, entry domain.Retry, delete bool) error {
	if taskKeys == nil || len(taskKeys) < 1 {
		return nil
	}

	m := make(map[string]any)
	if entry.Status != 0 {
		m["status"] = entry.Status
	}
	if delete {
		m["dtime"] = time.Now().UnixMilli()
	}

	err := d.db.WithContext(ctx).
		Model(Retry{}).
		Where("task_key in (?) and dtime = 0", taskKeys).
		Updates(m).
		Error
	if err != nil {
		return err
	}
	return nil
}

func (d *GORMRetryDAO) SelectByTaskKey(ctx context.Context, key string) (*Retry, error) {
	var rr Retry
	err := d.db.WithContext(ctx).
		First(&rr, "task_key = ? and dtime = 0", key).
		Error
	if err != nil {
		return nil, err
	}
	return &rr, nil
}

func (d *GORMRetryDAO) UpdateByTaskKeyAndOptionalDelete(ctx context.Context, taskKey string, entry domain.Retry, delete bool) error {
	m := make(map[string]any)
	if entry.Status != 0 {
		m["status"] = entry.Status
	}
	if delete {
		m["dtime"] = time.Now().UnixMilli()
	}

	err := d.db.WithContext(ctx).
		Where("task_key = ? and dtime = 0", taskKey).
		Updates(m).
		Error
	if err != nil {
		return err
	}
	return nil
}

func NewGORMRetryDAO(db *gorm.DB) RetryDAO {
	return &GORMRetryDAO{db: db}
}

func (d *GORMRetryDAO) Insert(ctx context.Context, model Retry) error {
	now := time.Now().UnixMilli()
	model.Ctime = now
	model.Utime = now
	model.Dtime = 0
	err := d.db.WithContext(ctx).Create(&model).Error
	return err
}
