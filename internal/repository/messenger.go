package repository

import (
	"context"
	"encoding/json"
	"gitee.com/aqualion/go-webbook/pkg/logger"
)

var (
	TopicSnapshotUpdated  = "SNAPSHOT_UPDATED"
	TopicInteractiveLiked = "INTERACTIVE_LIKED"
)

type Messenger interface {
	SendMsg(ctx context.Context, topic string, msg any) error
	AddSubscription(taskName, topic string, handler TaskHandler)
}

type SimpleMessenger struct {
	msgChan     chan Message
	taskMap     map[string][]Task
	taskNameMap map[string]bool
	l           logger.LoggerV1
}

type Task struct {
	handler TaskHandler
	name    string
}

type TaskHandler func(ctx context.Context, msg string) error

func NewSimpleMessenger(l logger.LoggerV1) *SimpleMessenger {
	instance := &SimpleMessenger{
		msgChan:     make(chan Message, 16),
		taskMap:     make(map[string][]Task),
		taskNameMap: make(map[string]bool),
		l:           l,
	}
	go instance.startListening()
	return instance
}

func (c *SimpleMessenger) startListening() {
	for {
		select {
		case m := <-c.msgChan:
			c.l.Info("消费到消息", logger.Any("topic", m.topic))
			tasks := c.taskMap[m.topic]
			if len(tasks) < 1 {
				return
			}
			for _, task := range tasks {
				go func(task Task) {
					c.l.Info("消费任务触发", logger.Any("task_name", task.name))
					err := task.handler(m.ctx, m.msg)
					if err != nil {
						c.l.Error("消费者发生错误",
							logger.Error(err),
							logger.Any("topic", m.topic),
							logger.Any("task", task.name),
						)
					}
				}(task)
			}
		default:
		}
	}
}

func (c *SimpleMessenger) SendMsg(ctx context.Context, topic string, msg any) error {
	marshal, err := json.Marshal(msg)
	if err != nil {
		return err
	}
	c.msgChan <- Message{
		topic: topic,
		msg:   string(marshal),
		ctx:   ctx,
	}
	return nil
}

func (c *SimpleMessenger) AddSubscription(taskName, topic string, handler TaskHandler) {
	_, exist := c.taskNameMap[taskName]
	if exist {
		panic("重复注册的task:" + taskName)
	}
	c.taskNameMap[taskName] = true
	c.taskMap[topic] = append(c.taskMap[topic], Task{
		handler: handler,
		name:    taskName,
	})
}

type Message struct {
	ctx   context.Context
	topic string
	msg   string
}

type MsgSnapshotUpdated struct {
	Biz     string `json:"biz"`
	CntType string `json:"cntType"`
	Limit   int    `json:"limit"`
}
type MsgInteractiveLiked struct {
	Biz     string `json:"biz"`
	CntType string `json:"cntType"`
	BizId   int64  `json:"biz_id"`
}
