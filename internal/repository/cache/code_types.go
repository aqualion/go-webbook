package cache

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
)

var (
	ErrCodeSendTooMany        = errors.New("发送验证码太频繁")
	ErrCodeVerifyTooManyTimes = errors.New("验证次数太多")
	ErrUnknownForCode         = errors.New("我也不知发生什么了，反正是跟 code 有关")
)

func makeKey(biz, phone string) string {
	return fmt.Sprintf("phone_code:%s:%s", biz, phone)
}

//go:generate mockgen -source ./code_types.go -package=cachemocks -destination=./mocks/code.mock.go CodeCache
type CodeCache interface {
	Set(ctx context.Context, biz, phone, code string) error
	Verify(ctx context.Context, biz, phone, inputCode string) (bool, error)
}
