package cache

import (
	"context"
	"fmt"
	"github.com/redis/go-redis/v9"
	"testing"
)

func TestZset(t *testing.T) {
	client := redis.NewClient(&redis.Options{Addr: "localhost:6379"})
	zsetKey := "go2zset"
	ranking := []redis.Z{
		{Score: 100.0, Member: "钟南山"},
		{Score: 80.0, Member: "林医生"},
		{Score: 70.0, Member: "王医生"},
		{Score: 75.0, Member: "张医生"},
		{Score: 59.0, Member: "叶医生"},
	}
	client.ZAdd(context.Background(), zsetKey, ranking...)
	//golang+5分
	newScoreResult, err := client.ZIncrBy(context.Background(), zsetKey, 5.0, "钟南山").Result()

	if err != nil {
		t.Fatal(err)
	}
	fmt.Println("钟南山加5分后的最新分数", newScoreResult)
	//取zset里的前2名热度的医生
	zsetList2, _ := client.ZRevRangeWithScores(context.Background(), zsetKey, 0, 10).Result()
	fmt.Println("zset前2名热度的医生", zsetList2)

	newRanking := []redis.Z{
		{Score: 200.0, Member: "钟南山"},
		{Score: 203.0, Member: "胡医生"},
		{Score: 70.0, Member: "王医生"},
		{Score: 75.0, Member: "张医生"},
		{Score: 59.0, Member: "叶医生"},
	}
	client.Del(context.Background(), zsetKey)
	client.ZAdd(context.Background(), zsetKey, newRanking...)
	zsetList3, _ := client.ZRevRangeWithScores(context.Background(), zsetKey, 0, 10).Result()
	fmt.Println("zset前5名热度的医生, new ranking", zsetList3)

	client.Del(context.Background(), zsetKey)
}
