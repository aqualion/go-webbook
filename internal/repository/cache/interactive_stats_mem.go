package cache

import (
	"context"
	"gitee.com/aqualion/go-webbook/internal/domain"
	"github.com/redis/go-redis/v9"
)

type InMemStatsCacher struct {
	redisClient redis.Cmdable
	data        map[string][]*domain.InteractiveStats
	CacherCommon
}

func NewInMemStatsCacher(cmdable redis.Cmdable, common CacherCommon) *InMemStatsCacher {
	return &InMemStatsCacher{redisClient: cmdable,
		data:         make(map[string][]*domain.InteractiveStats),
		CacherCommon: common,
	}
}

func (i *InMemStatsCacher) GetRank(ctx context.Context, biz, cntType string, limit int) ([]*domain.InteractiveStats, error) {
	key := makeStatsKey(biz, cntType, i.CacherCommon.KeyPrefix())
	stats, ok := i.data[key]
	if !ok {
		return []*domain.InteractiveStats{}, nil
	}
	return stats, nil
}

func (i *InMemStatsCacher) SaveRank(ctx context.Context, biz, cntType string, data []domain.InteractiveStats) error {
	if data == nil || len(data) < 1 {
		return nil
	}
	key := makeStatsKey(biz, cntType, i.CacherCommon.KeyPrefix())
	values := make([]*domain.InteractiveStats, 0, len(data))
	for _, datum := range data {
		values = append(values, &domain.InteractiveStats{
			Biz:     datum.Biz,
			BizId:   datum.BizId,
			Cnt:     datum.Cnt,
			CntType: datum.CntType,
		})
	}
	i.data[key] = values
	return nil
}
