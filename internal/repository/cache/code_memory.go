package cache

import (
	"context"
	_ "embed"
	goCache "github.com/patrickmn/go-cache"
	"strings"
	"sync"
	"time"
)

// LocalCodeCache 假如说你要切换这个，你是不是得把 lua 脚本的逻辑，在这里再写一遍？
// 这是第四周作业1
type LocalCodeCache struct {
	lc *goCache.Cache
	mu sync.Mutex
}

func NewLocalCodeCache(expDur time.Duration, cleanInterval time.Duration) CodeCache {
	return &LocalCodeCache{
		lc: goCache.New(expDur, cleanInterval),
		mu: sync.Mutex{},
	}
}

func (l *LocalCodeCache) Set(ctx context.Context, biz, phone, code string) error {
	key := makeKey(biz, phone)
	cntKey := key + ":cnt"
	l.mu.Lock()
	defer l.mu.Unlock()
	_, expiration, keyFound := l.lc.GetWithExpiration(key)

	// if 没有缓存 || 短信已发送超过1分钟
	if !keyFound || aMinuteElapsed(expiration, time.Now()) {
		// 设置缓存
		// 缓存过期时间设为10分钟
		l.lc.Set(key, code, 0)
		// 尝试计数设置为3次
		// 尝试计数过期时间设为10分钟
		l.lc.Set(cntKey, 3, 0)
		return nil
	}
	// else
	// 发送太频繁:ErrCodeVerifyTooManyTimes
	return ErrCodeSendTooMany
}

func (l *LocalCodeCache) Verify(ctx context.Context, biz, phone, inputCode string) (bool, error) {
	key := makeKey(biz, phone)
	cntKey := key + ":cnt"

	l.mu.Lock()
	defer l.mu.Unlock()
	cntVal, cntFound := l.lc.Get(cntKey)
	codeVal, codeFound := l.lc.Get(key)
	// 验证码已经过期，验证不通过
	if !cntFound || !codeFound || cntVal == nil || codeVal == nil {
		return false, nil
	}
	cnt := cntVal.(int)
	code := codeVal.(string)

	// 次数已耗尽
	if cnt <= 0 {
		return false, ErrCodeVerifyTooManyTimes
	}

	// 输入正确
	if strings.ToLower(code) == strings.ToLower(inputCode) {
		l.lc.Set(cntKey, -1, 0)
		return true, nil
	} else {
		// 用户输错了，可验证次数-1
		if err := l.lc.Decrement(cntKey, 1); err != nil {
			return false, err
		}
		return false, nil
	}
}

// aMinuteElapsed 用 expiration - current = 剩余时间, 如果剩余时间小于9分钟, 返回true, 否则返回false
func aMinuteElapsed(expiration time.Time, current time.Time) bool {
	if expiration.IsZero() {
		return false
	}
	dur := expiration.Sub(current)
	minLeft := dur.Minutes()
	return minLeft < 9
}
