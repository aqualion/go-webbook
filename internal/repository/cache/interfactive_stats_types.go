package cache

import (
	"context"
	_ "embed"
	"gitee.com/aqualion/go-webbook/internal/domain"
)

var (
	//go:embed lua/interative_stats_remake_zset.lua
	LuaStatsRemakeZset string
	//go:embed lua/interative_stats_incr_member_score.lua
	LuaStatsIncrMemberScore string
)

type RankCacherIncreaser interface {
	RankCacher
	RankIncreaser
}

type RankCacher interface {
	RankGetter
	RankSaver
	CacherCommon
}

type RankGetter interface {
	GetRank(ctx context.Context, biz, cntType string, limit int) ([]*domain.InteractiveStats, error)
}

type RankSaver interface {
	SaveRank(ctx context.Context, biz, cntType string, data []domain.InteractiveStats) error
}

type RankIncreaser interface {
	IncrRankIfPresent(ctx context.Context, biz, cntType string, bizId int64, score int) error
}

type CacherCommon interface {
	KeyPrefix() string
	Name() string
}

type CacherCommonImpl struct {
	keyPrefix string
	name      string
}

func NewCacherCommonImpl(keyPrefix string, name string) *CacherCommonImpl {
	return &CacherCommonImpl{keyPrefix: keyPrefix, name: name}
}

func (d *CacherCommonImpl) KeyPrefix() string {
	return d.keyPrefix
}

func (d *CacherCommonImpl) Name() string {
	return d.name
}
