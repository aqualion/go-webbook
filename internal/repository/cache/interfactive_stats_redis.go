package cache

import (
	"context"
	_ "embed"
	"errors"
	"fmt"
	"gitee.com/aqualion/go-webbook/internal/domain"
	"github.com/redis/go-redis/v9"
	"strconv"
)

type RedisZsetCacher struct {
	redisClient redis.Cmdable
	CacherCommon
}

func NewRedisZsetCacher(redisClient redis.Cmdable, common CacherCommon) *RedisZsetCacher {
	return &RedisZsetCacher{redisClient: redisClient, CacherCommon: common}
}

func (d *RedisZsetCacher) GetRank(ctx context.Context, biz, cntType string, limit int) ([]*domain.InteractiveStats, error) {
	called := d.redisClient.ZRevRangeWithScores(ctx, makeStatsKey(biz, cntType, d.CacherCommon.KeyPrefix()), 0, -1)
	result, err := called.Result()
	if err != nil {
		return []*domain.InteractiveStats{}, err
	}
	if result == nil || len(result) < 1 {
		return []*domain.InteractiveStats{}, nil
	}

	if len(result) > limit {
		result = result[:limit]
	}

	stats := make([]*domain.InteractiveStats, 0, len(result))
	for _, z := range result {
		score, err := strconv.Atoi(fmt.Sprintf("%v", z.Member))
		if err != nil {
			return nil, err
		}
		stats = append(stats, &domain.InteractiveStats{
			Biz:     biz,
			BizId:   int64(score),
			Cnt:     int64(z.Score),
			CntType: cntType,
		})
	}
	return stats, nil
}

func (d *RedisZsetCacher) SaveRank(ctx context.Context, biz, cntType string, data []domain.InteractiveStats) error {
	pairs := parseZsetPairs(data)
	if len(pairs)%2 != 0 {
		return errors.New("pairs输入解析错误")
	}
	return d.redisClient.Eval(ctx, LuaStatsRemakeZset, []string{makeStatsKey(biz, cntType, d.CacherCommon.KeyPrefix())}, pairs...).Err()
}

func (d *RedisZsetCacher) IncrRankIfPresent(ctx context.Context, biz, cntType string, bizId int64, score int) error {
	zsetKey := makeStatsKey(biz, cntType, d.CacherCommon.KeyPrefix())
	return d.redisClient.Eval(ctx, LuaStatsIncrMemberScore, []string{zsetKey}, score, bizId).Err()
}
