package cache

import (
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

// TestSendFirstTimeSuccess 测试没发送的情况下，发送应成功
func TestSendFirstTimeSuccess(t *testing.T) {
	cc := NewLocalCodeCache(10*time.Minute, 10*time.Minute)
	biz := defaultBiz()
	phone := defaultPhone()
	err := cc.Set(context.Background(), biz, phone, defaultCode())
	assert.Nil(t, nil, err)
}

// TestSendTooMany 测试发送太频繁
func TestSendTooMany(t *testing.T) {
	cc := NewLocalCodeCache(10*time.Minute, 10*time.Minute)
	biz := defaultBiz()
	phone := defaultPhone()
	err := cc.Set(context.Background(), biz, phone, defaultCode())
	assert.Nil(t, nil, err)

	err = cc.Set(context.Background(), biz, phone, defaultCode())
	assert.Error(t, err, ErrCodeVerifyTooManyTimes)
}

func defaultCode() string {
	return "123456"
}

func defaultPhone() string {
	return "10086"
}

func defaultBiz() string {
	return "login"
}

// Test_aMinuteElapsed 测试判断1分钟是否过去
func Test_aMinuteElapsed(t *testing.T) {
	type args struct {
		expiration time.Time
		current    time.Time
	}
	now := time.Now()
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "1分钟已过去, 返回true",
			args: args{
				expiration: now.Add(10 * time.Minute),
				current:    now.Add(61 * time.Second),
			},
			want: true,
		},
		{
			name: "1分钟还没过去, 返回false",
			args: args{
				expiration: now.Add(10 * time.Minute),
				current:    now.Add(60 * time.Second),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, aMinuteElapsed(tt.args.expiration, tt.args.current), "aMinuteElapsed(%v, %v)", tt.args.expiration, tt.args.current)
		})
	}
}

// TestVerifyCorrectInputTwice 测试verify: 发送验证码后，输入正确，此时再次输入应失败
func TestVerifyCorrectInputTwice(t *testing.T) {
	// 发送验证码
	cc := NewLocalCodeCache(10*time.Minute, 10*time.Minute)
	biz := defaultBiz()
	phone := defaultPhone()
	code := defaultCode()
	err := cc.Set(context.Background(), biz, phone, code)
	assert.Nil(t, nil, err)

	// 验证正确第一次
	r1, err := cc.Verify(context.Background(), biz, phone, code)
	assert.True(t, r1)
	assert.Nil(t, err)

	// 再次输入相同验证码应失败
	r2, err := cc.Verify(context.Background(), biz, phone, code)
	assert.False(t, r2)
	assert.Error(t, ErrCodeVerifyTooManyTimes)
}

// TestVerifyFailedThreeTimes 测试verify: 发送验证码后, 输入错误3次, 此时任务应失败
func TestVerifyFailedThreeTimes(t *testing.T) {
	// 发送验证码
	cc := NewLocalCodeCache(10*time.Minute, 10*time.Minute)
	biz := defaultBiz()
	phone := defaultPhone()
	code := defaultCode()
	incorrectCode := "incorrect_code"
	err := cc.Set(context.Background(), biz, phone, code)
	assert.Nil(t, nil, err)

	// 错误3次
	r, err := cc.Verify(context.Background(), biz, phone, incorrectCode)
	assert.False(t, r)
	assert.Nil(t, err)

	r, err = cc.Verify(context.Background(), biz, phone, incorrectCode)
	assert.False(t, r)
	assert.Nil(t, err)

	r, err = cc.Verify(context.Background(), biz, phone, incorrectCode)
	assert.False(t, r)
	assert.Nil(t, err)

	// 第4次次数耗尽
	r, err = cc.Verify(context.Background(), biz, phone, code)
	assert.False(t, r)
	assert.Error(t, ErrCodeVerifyTooManyTimes)
}

// TestVerifyExpiredCode 测试verify: 验证码已过期的情况下, 验证应失败
func TestVerifyExpiredCode(t *testing.T) {
	cc := NewLocalCodeCache(1*time.Second, 10*time.Minute)

	// 发送验证码
	biz := defaultBiz()
	phone := defaultPhone()
	code := defaultCode()
	err := cc.Set(context.Background(), biz, phone, code)
	assert.Nil(t, nil, err)

	// 等待2秒
	time.Sleep(2 * time.Second)
	r, err := cc.Verify(context.Background(), biz, phone, code)
	assert.False(t, r)
	assert.Nil(t, err)
}
