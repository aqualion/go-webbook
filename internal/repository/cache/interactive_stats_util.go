package cache

import (
	"fmt"
	"gitee.com/aqualion/go-webbook/internal/domain"
)

func makeStatsKey(biz, cntType, prefix string) string {
	if prefix == "" {
		return fmt.Sprintf("stats:%s:%s", biz, cntType)
	}
	return fmt.Sprintf("stats:%s:%s:%s", prefix, biz, cntType)
}

func parseZsetPairs(data []domain.InteractiveStats) []any {
	if data == nil || len(data) < 1 {
		return []any{}
	}
	result := make([]any, 0, len(data)*2)
	for _, datum := range data {
		result = append(result, datum.Cnt)
		result = append(result, fmt.Sprintf("%d", datum.BizId))
	}
	return result
}
