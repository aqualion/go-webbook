-- zset key
local key = KEYS[1]

-- increment
local delta = tonumber(ARGV[1])

-- member
local member = ARGV[2]

local exists = redis.call("EXISTS", key)
if exists == 1 then
    redis.call("ZINCRBY", key, delta, member)
    -- 说明自增成功了
    return 1
else
    -- 自增不成功
    return 0
end