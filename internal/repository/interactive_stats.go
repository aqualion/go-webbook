package repository

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/aqualion/go-webbook/internal/domain"
	"gitee.com/aqualion/go-webbook/internal/repository/cache"
	"gitee.com/aqualion/go-webbook/internal/repository/dao"
	"gitee.com/aqualion/go-webbook/pkg/logger"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
)

type InteractiveStatsRepository interface {
	GetRank(ctx context.Context, biz, cntType string, limit int) ([]*domain.InteractiveStats, error)
	Aggregate(ctx context.Context, biz, cntType string, limit int) error
	Snapshot(ctx context.Context, biz, cntType string, limit int) error
}

type InteractiveStatsMultiLayerCachedRepo struct {
	dao       dao.InteractiveStatsDAO
	dynamic   cache.RankCacherIncreaser
	snapshot  cache.RankCacher
	inMemory  cache.RankCacher
	cachers   []cache.RankCacher
	l         logger.LoggerV1
	messenger Messenger
}

func NewInteractiveStatsMultiLayerCachedRepo(
	dao dao.InteractiveStatsDAO,
	dynamic cache.RankCacherIncreaser,
	snapshot cache.RankCacher,
	inMemory cache.RankCacher,
	l logger.LoggerV1,
	messenger Messenger,
) *InteractiveStatsMultiLayerCachedRepo {
	instance := &InteractiveStatsMultiLayerCachedRepo{
		dao:       dao,
		dynamic:   dynamic,
		snapshot:  snapshot,
		inMemory:  inMemory,
		l:         l,
		cachers:   []cache.RankCacher{inMemory, snapshot, dynamic}, // 按这个顺序获取缓存
		messenger: messenger,
	}
	return instance.init()
}

func (i *InteractiveStatsMultiLayerCachedRepo) getTracer() trace.Tracer {
	return otel.GetTracerProvider().Tracer("InteractiveStats-MultiLayer-Cached-Repo")
}

func (i *InteractiveStatsMultiLayerCachedRepo) init() *InteractiveStatsMultiLayerCachedRepo {
	// 注册消费者：snapshot更新成功时，刷新本地缓存
	i.messenger.AddSubscription("refresh_local_cache", TopicSnapshotUpdated, func(ctx context.Context, msg string) error {
		ctx, span := i.getTracer().Start(ctx, "刷新本地缓存")
		defer span.End()
		var m MsgSnapshotUpdated
		err := json.Unmarshal([]byte(msg), &m)
		if err != nil {
			return err
		}

		span.AddEvent("从静态缓存读取数据")
		ranks, err := i.snapshot.GetRank(context.Background(), m.Biz, m.CntType, m.Limit)
		if err != nil {
			return err
		}
		if ranks == nil || len(ranks) < 1 {
			return errors.New("no snapshot found")
		}
		span.AddEvent("保存到本地缓存")
		return i.inMemory.SaveRank(context.Background(), m.Biz, m.CntType, conv2Value(ranks))
	})
	// 注册消费者: 点赞数变化时，更新dynamic缓存中的计数
	i.messenger.AddSubscription("incr_dynamic_cache", TopicInteractiveLiked, func(ctx context.Context, msg string) error {
		var m MsgInteractiveLiked
		err := json.Unmarshal([]byte(msg), &m)
		if err != nil {
			return err
		}
		return i.dynamic.IncrRankIfPresent(context.Background(), m.Biz, m.CntType, m.BizId, 1)
	})
	return i
}

func (i *InteractiveStatsMultiLayerCachedRepo) GetRank(ctx context.Context, biz, cntType string, limit int) ([]*domain.InteractiveStats, error) {
	tag := fmt.Sprintf("%v_%v_%v", biz, cntType, limit)
	for j, cacher := range i.cachers {
		result, err := cacher.GetRank(ctx, biz, cntType, limit)
		if err != nil || result == nil || len(result) < 1 {
			i.l.Info("从缓存获取统计数据发生错误, 或没有数据，尝试从下一层获取",
				logger.Any("缓存层级", cacher.Name()),
				logger.Any("error", err),
				logger.Any("请求", tag),
			)
			continue
		}
		i.l.Debug("成功获取到排名数据", logger.Any("缓存层级", cacher.Name()))
		// 如果不是从本地缓存获取到数据，则将获取到的数据放入本地缓存
		if j != 0 {
			go func() {
				err := i.inMemory.SaveRank(context.Background(), biz, cntType, conv2Value(result))
				if err != nil {
					i.l.Error("刷新本地缓存错误",
						logger.Error(err),
						logger.Any("biz", biz),
						logger.Any("cnt_type", cntType),
					)
				}
			}()
		}
		return result, nil
	}
	i.l.Warn("所有缓存均无数据，切换到从数据库获取",
		logger.Any("请求", tag),
	)
	data, err := i.dao.SelectByBiz(ctx, biz, cntType, limit)
	if err != nil {
		return nil, err
	}

	// 如果不是从本地缓存获取到数据，则将获取到的数据放入本地缓存
	if len(data) > 1 {
		go func() {
			err := i.inMemory.SaveRank(context.Background(), biz, cntType, conv2DomainValue(biz, cntType, data))
			if err != nil {
				i.l.Error("刷新本地缓存错误",
					logger.Error(err),
					logger.Any("biz", biz),
					logger.Any("cnt_type", cntType),
				)
			}
		}()
	}
	return conv2DomainPtr(biz, cntType, data), nil
}

func (i *InteractiveStatsMultiLayerCachedRepo) Aggregate(ctx context.Context, biz, cntType string, limit int) error {
	ctx, span := i.getTracer().Start(ctx, "聚合排行数据并放入动态缓存")
	defer span.End()
	span.AddEvent("查询数据库")
	aggregated, err := i.dao.CountAndGroupByBiz(ctx, biz, cntType, limit)
	if err != nil {
		return err
	}

	if len(aggregated) < 1 {
		return nil
	}

	span.AddEvent("将数据存入动态缓存")
	err = i.dynamic.SaveRank(ctx, biz, cntType, conv2DomainValue(biz, cntType, aggregated))
	if err != nil {
		return err
	}
	return nil
}

func (i *InteractiveStatsMultiLayerCachedRepo) Snapshot(ctx context.Context, biz, cntType string, limit int) error {
	ctx, span := i.getTracer().Start(ctx, "快照动态缓存数据并存入静态缓存")
	defer span.End()

	span.AddEvent("快照动态缓存")
	fromDynamic, err := i.dynamic.GetRank(ctx, biz, cntType, limit)
	if err != nil {
		return err
	}
	if fromDynamic == nil || len(fromDynamic) < 1 {
		return nil
	}
	span.AddEvent("存入静态缓存")
	err = i.snapshot.SaveRank(ctx, biz, cntType, conv2Value(fromDynamic))
	if err != nil {
		return err
	}
	go func() {
		span.AddEvent("发消息通知本地缓存更新")
		err := i.messenger.SendMsg(ctx, TopicSnapshotUpdated, MsgSnapshotUpdated{
			Biz:     biz,
			CntType: cntType,
			Limit:   limit,
		})
		if err != nil {
			i.l.Error("发送消息错误", logger.Error(err))
		}
	}()
	return nil
}

func conv2Value(input []*domain.InteractiveStats) []domain.InteractiveStats {
	if input == nil || len(input) < 1 {
		return []domain.InteractiveStats{}
	}
	result := make([]domain.InteractiveStats, 0, len(input))
	for _, e := range input {
		result = append(result, domain.InteractiveStats{
			Biz:     e.Biz,
			BizId:   e.BizId,
			Cnt:     e.Cnt,
			CntType: e.CntType,
		})
	}
	return result
}

func conv2DomainPtr(biz, cntType string, data []*dao.InteractiveStatsModel) []*domain.InteractiveStats {
	if data == nil || len(data) < 1 {
		return []*domain.InteractiveStats{}
	}
	result := make([]*domain.InteractiveStats, 0, len(data))
	for _, d := range data {
		result = append(result, &domain.InteractiveStats{
			Biz:     biz,
			BizId:   d.BizId,
			Cnt:     d.Cnt,
			CntType: cntType,
		})
	}
	return result
}

func conv2DomainValue(biz, cntType string, data []*dao.InteractiveStatsModel) []domain.InteractiveStats {
	if data == nil || len(data) < 1 {
		return []domain.InteractiveStats{}
	}
	result := make([]domain.InteractiveStats, 0, len(data))
	for _, d := range data {
		result = append(result, domain.InteractiveStats{
			Biz:     biz,
			BizId:   d.BizId,
			Cnt:     d.Cnt,
			CntType: cntType,
		})
	}
	return result
}
