package repository

import (
	"context"
	"errors"
	"fmt"
	"gitee.com/aqualion/go-webbook/pkg/logger"
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
)

func TestSimpleMessenger(t *testing.T) {
	var wg sync.WaitGroup
	wg.Add(2)
	messenger := NewSimpleMessenger(logger.NewNoOpLogger())
	topic1 := "testTopic1"
	topic2 := "testTopic2"
	messenger.AddSubscription("testTask", topic1, func(ctx context.Context, msg string) error {
		fmt.Println("消费到了msg:", msg)
		wg.Done()
		return nil
	})
	messenger.AddSubscription("testTask2", topic2, func(ctx context.Context, msg string) error {
		fmt.Println("消费到了msg:", msg, "现在模拟发生错误")
		wg.Done()
		return errors.New("这是一个模拟错误")
	})

	err := messenger.SendMsg(context.Background(), topic1, "随便发的消息1")
	assert.NoError(t, err)
	err = messenger.SendMsg(context.Background(), topic2, "随便发的消息2")
	assert.NoError(t, err)
	wg.Wait()
}
