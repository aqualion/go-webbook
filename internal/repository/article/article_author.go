package article

import (
	"context"
	"gitee.com/aqualion/go-webbook/internal/domain"
)

//go:generate mockgen -source=./article_author.go -package=repomocks -destination=../mocks/article.author.mock.go
type ArticleAuthorRepository interface {
	Create(ctx context.Context, art domain.Article) (int64, error)
	Update(ctx context.Context, art domain.Article) error
}
