package article

import (
	"context"
	"gitee.com/aqualion/go-webbook/internal/domain"
)

//go:generate mockgen -source=./article_reader.go -package=repomocks -destination=../mocks/article.reader.mock.go
type ArticleReaderRepository interface {
	// Save 有就更新，没有就新建，即 upsert 的语义
	Save(ctx context.Context, art domain.Article) (int64, error)
}
