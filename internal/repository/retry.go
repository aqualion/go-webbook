package repository

import (
	"context"
	"gitee.com/aqualion/go-webbook/internal/domain"
	"gitee.com/aqualion/go-webbook/internal/repository/dao"
)

var ErrRetryDuplicate = dao.ErrRetryDuplicate

//go:generate mockgen -source=./retry.go -package=repomocks -destination=./mocks/retry.mock.go
type RetryRepository interface {
	Save(ctx context.Context, entry domain.Retry) error
	GetByTaskKey(ctx context.Context, taskKey string) (*domain.Retry, error)
	UpdateByTaskKeyAndOptionalDelete(ctx context.Context, taskKey string, entry domain.Retry, delete bool) error
	BatchUpdateByTaskKeyAndOptionalDelete(ctx context.Context, taskKey []string, entry domain.Retry, delete bool) error
	ListValid(ctx context.Context) ([]*domain.Retry, error)
}

type RetryRepositoryImpl struct {
	dao dao.RetryDAO
}

func (r *RetryRepositoryImpl) BatchUpdateByTaskKeyAndOptionalDelete(ctx context.Context, taskKeys []string, entry domain.Retry, delete bool) error {
	return r.dao.BatchUpdateByTaskKeyAndOptionalDelete(ctx, taskKeys, entry, delete)
}

func (r *RetryRepositoryImpl) ListValid(ctx context.Context) ([]*domain.Retry, error) {
	list, err := r.dao.ListValid(ctx)
	if err != nil {
		return []*domain.Retry{}, err
	}
	if list == nil || len(list) < 1 {
		return []*domain.Retry{}, nil
	}
	mappedList := make([]*domain.Retry, len(list))
	for i, e := range list {
		mappedList[i] = &domain.Retry{
			Limit:         e.Limit,
			IntervalMilli: e.IntervalMilli,
			Type:          e.Type,
			TaskKey:       e.TaskKey,
			RetryRequest:  e.RetryRequest,
			Reason:        e.Reason,
			Status:        e.Status,
			ExpireTime:    e.ExpireTime,
		}
	}

	return mappedList, nil
}

func (r *RetryRepositoryImpl) GetByTaskKey(ctx context.Context, taskKey string) (*domain.Retry, error) {
	rr, err := r.dao.SelectByTaskKey(ctx, taskKey)
	if err != nil {
		return nil, err
	}
	return &domain.Retry{
		Limit:         rr.Limit,
		IntervalMilli: rr.IntervalMilli,
		Type:          rr.Type,
		TaskKey:       rr.TaskKey,
		RetryRequest:  rr.RetryRequest,
		Reason:        rr.Reason,
		Status:        rr.Status,
	}, nil
}

func (r *RetryRepositoryImpl) UpdateByTaskKeyAndOptionalDelete(ctx context.Context, taskKey string, entry domain.Retry, delete bool) error {
	return r.dao.UpdateByTaskKeyAndOptionalDelete(ctx, taskKey, entry, delete)
}

func NewRetryRepository(dao dao.RetryDAO) RetryRepository {
	return &RetryRepositoryImpl{dao: dao}
}

func (r *RetryRepositoryImpl) Save(ctx context.Context, do domain.Retry) error {
	return r.dao.Insert(ctx, dao.Retry{
		Limit:         do.Limit,
		IntervalMilli: do.IntervalMilli,
		Type:          do.Type,
		Status:        do.Status,
		RetryRequest:  do.RetryRequest,
		Reason:        do.Reason,
		TaskKey:       do.TaskKey,
		ExpireTime:    do.ExpireTime,
	})
}
