package ioc

import (
	"gitee.com/aqualion/go-webbook/internal/repository/cache"
	"time"
)

func InitCodeCache() cache.CodeCache {
	return cache.NewLocalCodeCache(10*time.Minute, 10*time.Minute)
}
