package ioc

import (
	"gitee.com/aqualion/go-webbook/conf"
	"gitee.com/aqualion/go-webbook/internal/repository"
	"gitee.com/aqualion/go-webbook/internal/schedule"
	"gitee.com/aqualion/go-webbook/internal/service/sms"
	"gitee.com/aqualion/go-webbook/internal/service/sms/async"
	"gitee.com/aqualion/go-webbook/internal/service/sms/memory"
	"gitee.com/aqualion/go-webbook/pkg/avail"
	ratelimit2 "gitee.com/aqualion/go-webbook/pkg/ratelimit"
	"github.com/redis/go-redis/v9"
	"time"
)

func InitSMSService(cmd redis.Cmdable) sms.IdableService {
	// 换内存，还是换别的
	//svc := ratelimit.NewRatelimitSMSService(memory.NewService(),
	//	limiter.NewRedisSlidingWindowLimiter(cmd, time.Second, 100))
	//return retryable.NewService(svc, 3)
	//return async.NewRetryableSMSService()
	return memory.NewInMemorySmsService()
}

func InitSMSServiceInMemory(cmd redis.Cmdable) sms.Service {
	// 换内存，还是换别的
	//svc := ratelimit.NewRatelimitSMSService(memory.NewService(),
	//	limiter.NewRedisSlidingWindowLimiter(cmd, time.Second, 100))
	//return retryable.NewService(svc, 3)
	//return async.NewRetryableSMSService()
	return memory.NewInMemorySmsService()
}

func InitSMSServiceAsync(
	redisCmd redis.Cmdable,
	svc sms.IdableService,
	retryRepo repository.RetryRepository,
) sms.Service {
	limiter := ratelimit2.NewRedisSlidingWindowLimiter(redisCmd, time.Second, 1000)
	avChecker := avail.NewRedisMetricsChecker(redisCmd, avail.RedisMetricsCheckerConf{
		TargetKey:        "metrics:in-memory-sms-service",
		ThresholdPercent: 70,
	})
	scheduler := schedule.NewSmsScheduler(retryRepo)

	return async.NewRetryableSMSService(
		svc,
		limiter,
		avChecker,
		retryRepo,
		conf.RetryConfig{
			Type:          "sms-send",
			IntervalMilli: 1000 * 30,
			Limit:         3,
		},
		scheduler.Schedule())
}
