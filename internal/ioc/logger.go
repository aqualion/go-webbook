package ioc

import (
	"gitee.com/aqualion/go-webbook/pkg/logger"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

func InitLogger() logger.LoggerV1 {
	// 这里我们用一个小技巧，
	// 就是直接使用 zap 本身的配置结构体来处理
	//cfg := zap.NewDevelopmentConfig()
	cfg := zap.NewProductionConfig()
	//cfg.Level = zap.NewAtomicLevelAt(zap.InfoLevel)
	err := viper.UnmarshalKey("log", &cfg)
	if err != nil {
		panic(err)
	}
	l, err := cfg.Build()
	if err != nil {
		panic(err)
	}
	zap.ReplaceGlobals(l)
	return logger.NewZapLogger(l)
}
