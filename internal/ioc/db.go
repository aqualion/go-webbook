package ioc

import (
	"fmt"
	"gitee.com/aqualion/go-webbook/conf"
	"gitee.com/aqualion/go-webbook/internal/repository/dao"
	"gitee.com/aqualion/go-webbook/pkg/gormx"
	"gitee.com/aqualion/go-webbook/pkg/logger"
	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	glogger "gorm.io/gorm/logger"
	"gorm.io/plugin/opentelemetry/tracing"
	gormProm "gorm.io/plugin/prometheus"
)

func InitDB(l logger.LoggerV1) *gorm.DB {
	// 读取db配置
	c := conf.WebookConfig{}
	err := viper.Unmarshal(&c)
	if err != nil {
		panic(fmt.Errorf("初始化配置失败 %v, 原因 %w", c, err))
	}
	config := glogger.Config{
		SlowThreshold: 0,
		LogLevel:      glogger.Info,
	}

	// 初始化db
	db, err := gorm.Open(mysql.Open(c.DB.DSN), &gorm.Config{
		// 使用 DEBUG 来打印
		Logger: glogger.New(gormLoggerFunc(l.Debug), config),
	})
	if err != nil {
		panic(err)
	}

	/**
	接入prometheus start
	*/
	// 接入gorm框架自带的监控
	err = db.Use(gormProm.New(gormProm.Config{
		DBName: "webook",
		// 每 15 秒采集一些数据
		RefreshInterval: 15,
		MetricsCollector: []gormProm.MetricsCollector{
			&gormProm.MySQL{
				VariableNames: []string{"Threads_running"},
			},
		}, // user defined metrics
	}))
	if err != nil {
		panic(err)
	}

	// 接入自定义的对语句执行时间的监控
	prom := gormx.PrometheusMetricsCallbacks{
		Namespace:  "aqualion",
		Subsystem:  "webook",
		Name:       "gorm_statement_execution_time",
		InstanceID: "my-instance-2",
		Help:       "gorm DB 查询",
	}
	err = prom.Register(db)
	if err != nil {
		panic(err)
	}

	/**
	接入prometheus end
	*/

	/**
	接入otel tracing start
	*/

	err = db.Use(tracing.NewPlugin(tracing.WithoutMetrics(),
		tracing.WithoutQueryVariables(),
	))
	if err != nil {
		panic(err)
	}

	/**
	接入otel tracing end
	*/

	// 自动建表
	err = dao.InitTable(db)
	if err != nil {
		panic(err)
	}
	return db
}

type gormLoggerFunc func(msg string, fields ...logger.Field)

func (g gormLoggerFunc) Printf(msg string, args ...interface{}) {
	g(msg, logger.Field{Key: "args", Value: args})
}
