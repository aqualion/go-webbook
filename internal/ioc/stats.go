package ioc

import (
	"gitee.com/aqualion/go-webbook/internal/repository"
	"gitee.com/aqualion/go-webbook/internal/repository/cache"
	"gitee.com/aqualion/go-webbook/internal/repository/dao"
	"gitee.com/aqualion/go-webbook/internal/service"
	"gitee.com/aqualion/go-webbook/pkg/logger"
	"github.com/redis/go-redis/v9"
)

func InitStatsServiceConf() service.StatsServiceConf {
	return service.StatsServiceConf{
		Biz:                     "article",
		CntType:                 "like",
		Limit:                   1000,
		AggregationIntervalSecs: 3 * 60,
		SnapshotIntervalSecs:    20,
	}
}

func InitInteractiveStatsRepo(dao dao.InteractiveStatsDAO, cmdable redis.Cmdable, l logger.LoggerV1, messenger repository.Messenger) repository.InteractiveStatsRepository {
	dynamic := cache.NewRedisZsetCacher(cmdable, cache.NewCacherCommonImpl("dynamic", "dynamic-cacher"))
	snapshot := cache.NewRedisZsetCacher(cmdable, cache.NewCacherCommonImpl("snapshot", "snapshot-cacher"))
	inMem := cache.NewInMemStatsCacher(cmdable, cache.NewCacherCommonImpl("inmem", "inmem-cacher"))
	return repository.NewInteractiveStatsMultiLayerCachedRepo(dao, dynamic, snapshot, inMem, l, messenger)
}

func InitMessenger(l logger.LoggerV1) repository.Messenger {
	return repository.NewSimpleMessenger(l)
}
