package service

import (
	"context"
	"gitee.com/aqualion/go-webbook/internal/domain"
	"gitee.com/aqualion/go-webbook/internal/repository"
	"gitee.com/aqualion/go-webbook/internal/schedule"
	"gitee.com/aqualion/go-webbook/pkg/logger"
)

type InteractiveStatsService interface {
	GetRank(ctx context.Context, biz, cntType string, limit int) ([]*domain.InteractiveStats, error)
}

type StatsServiceConf struct {
	Biz                     string
	CntType                 string
	Limit                   int
	AggregationIntervalSecs int64
	SnapshotIntervalSecs    int64
}

type InteractiveStatsServiceImpl struct {
	repo       repository.InteractiveStatsRepository
	schManager schedule.MultiScheduler
	conf       StatsServiceConf
	l          logger.LoggerV1
}

func NewInteractiveStatsServiceImpl(
	repo repository.InteractiveStatsRepository,
	conf StatsServiceConf,
	l logger.LoggerV1,
) InteractiveStatsService {
	i := &InteractiveStatsServiceImpl{repo: repo, schManager: schedule.NewMultiScheduleManager(), conf: conf, l: l}
	i.schManager.AddScheduler(conf.AggregationIntervalSecs, func() {
		err := repo.Aggregate(context.Background(), conf.Biz, conf.CntType, conf.Limit)
		if err != nil {
			l.Error("聚合统计发生错误",
				logger.Any("biz", conf.Biz),
				logger.Any("cnt_type", conf.CntType),
				logger.Error(err),
			)
		}

	})
	i.schManager.AddScheduler(conf.SnapshotIntervalSecs, func() {
		err := repo.Snapshot(context.Background(), conf.Biz, conf.CntType, conf.Limit)
		if err != nil {
			l.Error("快照统计发生错误",
				logger.Field{
					Key:   "biz",
					Value: conf.Biz,
				},
				logger.Field{
					Key:   "cnt_type",
					Value: conf.CntType,
				},
				logger.Field{
					Key:   "error",
					Value: err,
				},
			)
		}
	})
	i.schManager.Schedule()
	return i
}

func (s *InteractiveStatsServiceImpl) GetRank(ctx context.Context, biz, cntType string, limit int) ([]*domain.InteractiveStats, error) {
	return s.repo.GetRank(ctx, biz, cntType, limit)
}
