package memory

import (
	"context"
	"fmt"
	"gitee.com/aqualion/go-webbook/internal/service/sms"
)

type InMemorySmsService struct {
}

func NewInMemorySmsService() sms.IdableService {
	return &InMemorySmsService{}
}

func (s *InMemorySmsService) GetServiceId() string {
	return "in-memory-sms-service"
}

func (s *InMemorySmsService) Send(ctx context.Context, tpl string, args []string, numbers ...string) error {
	fmt.Println(args)
	return nil
}
