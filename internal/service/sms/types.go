package sms

import (
	"context"
)

//go:generate mockgen -source=./types.go -package=svcmocks -destination=../mocks/sms.mock.go
type IdableService interface {
	Service
	Idable
}

type Idable interface {
	GetServiceId() string
}

type Service interface {
	// Send biz 很含糊的业务
	Send(ctx context.Context, biz string, args []string, numbers ...string) error
	//SendV1(ctx context.Context, tpl string, args []NamedArg, numbers ...string) error
	// 调用者需要知道实现者需要什么类型的参数，是 []string，还是 map[string]string
	//SendV2(ctx context.Context, tpl string, args any, numbers ...string) error
	//SendVV3(ctx context.Context, tpl string, args T, numbers ...string) error
}

type NamedArg struct {
	Val  string
	Name string
}
