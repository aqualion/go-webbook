package async

import (
	"context"
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gitee.com/aqualion/go-webbook/conf"
	"gitee.com/aqualion/go-webbook/internal/domain"
	"gitee.com/aqualion/go-webbook/internal/repository"
	"gitee.com/aqualion/go-webbook/internal/schedule"
	"gitee.com/aqualion/go-webbook/internal/service/sms"
	"gitee.com/aqualion/go-webbook/pkg/avail"
	"gitee.com/aqualion/go-webbook/pkg/ratelimit"
	"github.com/pkg/errors"
	"time"
)

var errLimited = fmt.Errorf("触发了限流")
var errFailSafeFailed = fmt.Errorf("存储重试请求失败")
var errDownStreamServiceNotAvailable = fmt.Errorf("下游服务不可用")

// RetryableSMSService 在被限流或检测到服务商状态异常时，会将请求存储到数据库，并由定时任务来进行延时重试
type RetryableSMSService struct {
	svc       sms.IdableService
	limiter   ratelimit.Limiter
	avChecker avail.AvailabilityChecker
	retryRepo repository.RetryRepository
	retryConf conf.RetryConfig
	scheduler schedule.Scheduler
}

type RetrySmsSendRequest struct {
	Tpl     string   `json:"tpl"`
	Args    []string `json:"args"`
	Numbers []string `json:"numbers"`
}

func NewRetryableSMSService(
	svc sms.IdableService,
	limiter ratelimit.Limiter,
	avChecker avail.AvailabilityChecker,
	retryRepo repository.RetryRepository,
	retryConf conf.RetryConfig,
	scheduler schedule.Scheduler,
) sms.Service {
	return &RetryableSMSService{
		svc:       svc,
		limiter:   limiter,
		avChecker: avChecker,
		retryRepo: retryRepo,
		retryConf: retryConf,
		scheduler: scheduler,
	}
}

func (s *RetryableSMSService) Send(ctx context.Context, tpl string, args []string, numbers ...string) error {
	limiterKey := "sms:limiter:" + s.svc.GetServiceId()
	limited, err := s.limiter.Limit(ctx, limiterKey)
	if err != nil {
		return fmt.Errorf("短信服务判断是否限流出现问题，%w", err)
	}

	// 如果被限流，触发同步转异步
	if limited {
		err := s.failSafe(ctx, RetrySmsSendRequest{
			Tpl:     tpl,
			Args:    args,
			Numbers: numbers,
		}, "限流触发")
		if err != nil {
			return errors.Wrap(err, errFailSafeFailed.Error())
		}
		return errLimited
	}

	// 如果检测到下游服务的可用性有问题，触发同步转异步
	availabilityResult, err := s.avChecker.CheckAvailability()
	if err != nil {
		return fmt.Errorf("短信服务判断服务商可用性出现问题，%w", err)
	}
	if !availabilityResult.Available {
		err := s.failSafe(ctx, RetrySmsSendRequest{
			Tpl:     tpl,
			Args:    args,
			Numbers: numbers,
		}, "服务可用性检测失败: "+availabilityResult.Reason)
		if err != nil {
			return errors.Wrap(err, errFailSafeFailed.Error())
		}
		return errDownStreamServiceNotAvailable
	}

	// 如果没问题，正常发送同步请求
	err = s.svc.Send(ctx, tpl, args, numbers...)
	return err
}

// failSafe 会将请求存储到数据库，后续交给定时任务来重试
func (s *RetryableSMSService) failSafe(
	ctx context.Context,
	request RetrySmsSendRequest,
	reason string,
) error {
	marshal, err := json.Marshal(request)
	if err != nil {
		return err
	}
	taskType := s.retryConf.Type
	limit := s.retryConf.Limit
	intervalMilli := s.retryConf.IntervalMilli

	reqStr := string(marshal)
	taskKey := hash(taskType + reqStr)

	existingRetry, err := s.retryRepo.GetByTaskKey(ctx, taskKey)
	if err != nil {
		return err
	}

	// 说明已经有重试任务在进行中了, 这里就不用重复发起了, 如果服务挂了，则在启动时再扫描retry表把没重试完的任务再启起来
	if existingRetry != nil {
		return nil
	}

	newRetry := domain.Retry{
		Limit:         limit,
		IntervalMilli: intervalMilli,
		Type:          taskType,
		RetryRequest:  reqStr,
		TaskKey:       taskKey,
		Reason:        reason,
		Status:        domain.StatusRetrying,
		ExpireTime:    time.Now().Add(parseExpireDuration(limit, intervalMilli, 2)).UnixMilli(),
	}
	err = s.retryRepo.Save(ctx, newRetry)
	// 并发情况下，如果一个线程因为唯一索引重复而创建失败，则代表其他线程已经发起了重试，这里就不需要重复发起重试
	if err != nil {
		return err
	}

	go s.Retry(ctx, request, taskKey, limit, intervalMilli)

	return nil
}

// 过期时间 = 当前时间 + (重试间隔 * 最大重试数 * 系数,根据实际情况调整)
func parseExpireDuration(limit int, milli int64, factor int) time.Duration {
	return time.Duration(limit) * time.Duration(milli) * time.Duration(factor) * time.Millisecond
}

func (s *RetryableSMSService) Retry(
	ctx context.Context,
	req RetrySmsSendRequest,
	taskKey string,
	limit int,
	intervalMilli int64,
) {
	for i := 0; i < limit; i++ {
		err := s.svc.Send(ctx, req.Tpl, req.Args, req.Numbers...)
		if err != nil {
			fmt.Println("重试发送短信发生错误:", err.Error())
			fmt.Printf("等待%v毫秒后再次重试...\n", intervalMilli)
			d := time.Duration(intervalMilli) * time.Millisecond
			time.Sleep(d)
			continue
		}
		// 到这里证明发送成功
		err = s.retryRepo.UpdateByTaskKeyAndOptionalDelete(ctx, taskKey, domain.Retry{
			Status: domain.StatusSuccess,
		}, true)
		if err != nil {
			fmt.Printf("更新retry失败: %v\n", err)
			return
		}
		return
	}
	// 到这里证明超过循环次数失败
	err := s.retryRepo.UpdateByTaskKeyAndOptionalDelete(ctx, taskKey, domain.Retry{
		Status: domain.StatusFailed,
	}, true)
	if err != nil {
		fmt.Printf("更新retry失败: %v\n", err)
		return
	}

}

func hash(s string) string {
	hasher := sha1.New()
	hasher.Write([]byte(s))
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	return sha
}
