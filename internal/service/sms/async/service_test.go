package async

import (
	"context"
	"errors"
	"gitee.com/aqualion/go-webbook/conf"
	"gitee.com/aqualion/go-webbook/internal/repository"
	repomocks "gitee.com/aqualion/go-webbook/internal/repository/mocks"
	"gitee.com/aqualion/go-webbook/internal/schedule"
	schmocks "gitee.com/aqualion/go-webbook/internal/schedule/mocks"
	svcmocks "gitee.com/aqualion/go-webbook/internal/service/mocks"
	"gitee.com/aqualion/go-webbook/internal/service/sms"
	"gitee.com/aqualion/go-webbook/pkg/avail"
	availmocks "gitee.com/aqualion/go-webbook/pkg/avail/mocks"
	"gitee.com/aqualion/go-webbook/pkg/ratelimit"
	limitmocks "gitee.com/aqualion/go-webbook/pkg/ratelimit/mocks"
	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"testing"
	"time"
)

func TestSend(t *testing.T) {
	testCases := []struct {
		name          string
		mockRetryRepo func(ctrl *gomock.Controller) repository.RetryRepository
		mockSvc       func(ctrl *gomock.Controller) sms.IdableService
		mockLimiter   func(ctrl *gomock.Controller) ratelimit.Limiter
		mockAvChecker func(ctrl *gomock.Controller) avail.AvailabilityChecker
		mockScheduler func(ctrl *gomock.Controller) schedule.Scheduler

		// 输入
		retryConfig conf.RetryConfig
		biz         string
		args        []string
		numbers     []string

		// 输出
		wantErr error
	}{
		{
			name: "测试：触发了限流",
			mockRetryRepo: func(ctrl *gomock.Controller) repository.RetryRepository {
				m := repomocks.NewMockRetryRepository(ctrl)
				m.EXPECT().GetByTaskKey(gomock.Any(), gomock.Any()).Return(nil, nil)
				m.EXPECT().Save(gomock.Any(), gomock.Any()).Return(nil)
				m.EXPECT().UpdateByTaskKeyAndOptionalDelete(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil)
				//...
				return m
			},
			mockSvc: func(ctrl *gomock.Controller) sms.IdableService {
				m := svcmocks.NewMockIdableService(ctrl)
				m.EXPECT().GetServiceId().Return("test-sms-service")
				m.EXPECT().Send(gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("假装失败1"))
				m.EXPECT().Send(gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("假装失败2"))
				m.EXPECT().Send(gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("假装失败3"))
				return m
			},
			mockLimiter: func(ctrl *gomock.Controller) ratelimit.Limiter {
				m := limitmocks.NewMockLimiter(ctrl)
				m.EXPECT().Limit(gomock.Any(), gomock.Any()).Return(true, nil)
				return m
			},
			mockAvChecker: func(ctrl *gomock.Controller) avail.AvailabilityChecker {
				m := availmocks.NewMockAvailabilityChecker(ctrl)
				return m
			},
			mockScheduler: func(ctrl *gomock.Controller) schedule.Scheduler {
				m := schmocks.NewMockScheduler(ctrl)
				return m
			},
			retryConfig: conf.RetryConfig{
				Type:          "sms-send",
				IntervalMilli: 1,
				Limit:         3,
			},
			biz:     "",
			args:    nil,
			numbers: nil,
			wantErr: errLimited,
		},
		{
			name: "测试：检测到服务不可用",
			mockRetryRepo: func(ctrl *gomock.Controller) repository.RetryRepository {
				m := repomocks.NewMockRetryRepository(ctrl)
				m.EXPECT().GetByTaskKey(gomock.Any(), gomock.Any()).Return(nil, nil)
				m.EXPECT().Save(gomock.Any(), gomock.Any()).Return(nil)
				m.EXPECT().UpdateByTaskKeyAndOptionalDelete(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil)
				//...
				return m
			},
			mockSvc: func(ctrl *gomock.Controller) sms.IdableService {
				m := svcmocks.NewMockIdableService(ctrl)
				m.EXPECT().GetServiceId().Return("test-sms-service")
				m.EXPECT().Send(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil)
				return m
			},
			mockLimiter: func(ctrl *gomock.Controller) ratelimit.Limiter {
				m := limitmocks.NewMockLimiter(ctrl)
				m.EXPECT().Limit(gomock.Any(), gomock.Any()).Return(false, nil)
				return m
			},
			mockAvChecker: func(ctrl *gomock.Controller) avail.AvailabilityChecker {
				m := availmocks.NewMockAvailabilityChecker(ctrl)
				m.EXPECT().CheckAvailability().Return(avail.AvailabilityResult{
					Available: false,
					Reason:    "cpu不足",
				}, nil)
				return m
			},
			mockScheduler: func(ctrl *gomock.Controller) schedule.Scheduler {
				m := schmocks.NewMockScheduler(ctrl)
				return m
			},
			retryConfig: conf.RetryConfig{
				Type:          "sms-send",
				IntervalMilli: 1,
				Limit:         3,
			},
			biz:     "",
			args:    nil,
			numbers: nil,
			wantErr: errDownStreamServiceNotAvailable,
		},
		{
			name: "测试：一切正常，走同步调用",
			mockRetryRepo: func(ctrl *gomock.Controller) repository.RetryRepository {
				m := repomocks.NewMockRetryRepository(ctrl)
				//m.EXPECT().GetByTaskKey(gomock.Any(), gomock.Any()).Return(nil, nil)
				//m.EXPECT().Save(gomock.Any(), gomock.Any()).Return(nil)
				//m.EXPECT().UpdateByTaskKeyAndOptionalDelete(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil)
				//...
				return m
			},
			mockSvc: func(ctrl *gomock.Controller) sms.IdableService {
				m := svcmocks.NewMockIdableService(ctrl)
				m.EXPECT().GetServiceId().Return("test-sms-service")
				m.EXPECT().Send(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil)
				return m
			},
			mockLimiter: func(ctrl *gomock.Controller) ratelimit.Limiter {
				m := limitmocks.NewMockLimiter(ctrl)
				m.EXPECT().Limit(gomock.Any(), gomock.Any()).Return(false, nil)
				return m
			},
			mockAvChecker: func(ctrl *gomock.Controller) avail.AvailabilityChecker {
				m := availmocks.NewMockAvailabilityChecker(ctrl)
				m.EXPECT().CheckAvailability().Return(avail.AvailabilityResult{
					Available: true,
					Reason:    "",
				}, nil)
				return m
			},
			mockScheduler: func(ctrl *gomock.Controller) schedule.Scheduler {
				m := schmocks.NewMockScheduler(ctrl)
				return m
			},
			retryConfig: conf.RetryConfig{
				Type:          "sms-send",
				IntervalMilli: 1,
				Limit:         3,
			},
			biz:     "",
			args:    nil,
			numbers: nil,
			wantErr: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			svc := NewRetryableSMSService(
				tc.mockSvc(ctrl),
				tc.mockLimiter(ctrl),
				tc.mockAvChecker(ctrl),
				tc.mockRetryRepo(ctrl),
				tc.retryConfig,
				tc.mockScheduler(ctrl),
			)
			err := svc.Send(context.Background(), tc.biz, tc.args, tc.numbers...)
			time.Sleep(time.Second)
			assert.ErrorIs(t, tc.wantErr, err)
		})
	}
}
