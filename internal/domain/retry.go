package domain

type Retry struct {
	Limit         int    // 最大重试数, 超过后放弃重试
	IntervalMilli int64  // 重试间隔时间(毫秒)
	Type          string // 标志是什么类型的重试任务, 如:send-sms
	TaskKey       string // task唯一标志符
	RetryRequest  string // json字符串，记录重试请求
	Reason        string // 为什么触发了重试
	Status        int    // 0: 重试中, 1: 重试失败, 2: 重试成功
	ExpireTime    int64  // 过期时间
}

const (
	StatusRetrying int = iota + 1
	StatusFailed
	StatusSuccess
)
