package domain

import "time"

type User struct {
	Id       int64
	Email    string
	Nickname string
	Password string
	Phone    string
	AboutMe  string
	Ctime    time.Time
	Birthday time.Time

	// 不要使用组合，因为你将来可能还有 DingDingInfo 之类的
	WechatInfo WechatInfo
}
