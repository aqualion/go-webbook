package schedule

import (
	"github.com/go-co-op/gocron"
	"time"
)

type MultiScheduleManager struct {
	schedulers []*gocron.Scheduler
}

func NewMultiScheduleManager() *MultiScheduleManager {
	return &MultiScheduleManager{schedulers: []*gocron.Scheduler{}}
}

func (s *MultiScheduleManager) AddScheduler(intervalSecs int64, doFunc func()) {
	scheduler := gocron.NewScheduler(time.UTC)
	_, schErr := scheduler.Every(int(intervalSecs)).Seconds().Do(doFunc)
	if schErr != nil {
		panic(schErr)
	}
	s.schedulers = append(s.schedulers, scheduler)
}

func (s *MultiScheduleManager) Schedule() MultiScheduler {
	for _, scheduler := range s.schedulers {
		scheduler.StartAsync()
	}
	return s
}
