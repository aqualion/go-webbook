package schedule

import (
	"context"
	"fmt"
	"gitee.com/aqualion/go-webbook/internal/domain"
	"gitee.com/aqualion/go-webbook/internal/repository"
	"github.com/go-co-op/gocron"
	"time"
)

// SmsScheduler 这个定时任务应该让分布式定时任务管理器来调度，确保每次只调度到一个实例上，目前暂时假定我们就单机运行
type SmsScheduler struct {
	scheduler *gocron.Scheduler
	retryRepo repository.RetryRepository
}

func NewSmsScheduler(retryRepo repository.RetryRepository) *SmsScheduler {
	return &SmsScheduler{retryRepo: retryRepo, scheduler: gocron.NewScheduler(time.UTC)}
}

func (s *SmsScheduler) Schedule() Scheduler {
	_, err := s.scheduler.Every(300).Seconds().Do(func() {
		entries, err := s.retryRepo.ListValid(context.Background())
		if err != nil {
			fmt.Println("检查retry发生错误:", err)
			return
		}
		now := time.Now().UnixMilli()

		var keys []string
		for _, entry := range entries {
			if now > entry.ExpireTime {
				keys = append(keys, entry.TaskKey)
			}
		}
		if len(keys) < 1 {
			return
		}
		fmt.Printf("找到%v个过期任务，开始刷新\n", len(keys))
		err = s.retryRepo.BatchUpdateByTaskKeyAndOptionalDelete(context.Background(), keys, domain.Retry{
			Status: domain.StatusFailed,
		}, true)
		if err != nil {
			fmt.Println("批量更新retry状态失败:", err)
			return
		}
	})
	if err != nil {
		panic(err)
	}
	s.scheduler.StartAsync()
	return s
}
