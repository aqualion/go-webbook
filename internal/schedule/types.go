package schedule

//go:generate mockgen -source=./types.go -package=schmocks -destination=./mocks/scheduler.mock.go
type Scheduler interface {
	Schedule() Scheduler
}

//go:generate mockgen -source=./types.go -package=schmocks -destination=./mocks/multi.scheduler.mock.go
type MultiScheduler interface {
	Schedule() MultiScheduler
	AddScheduler(intervalSecs int64, doFunc func())
}
