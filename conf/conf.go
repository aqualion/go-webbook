package conf

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
	"log/slog"
	"sync"
	"time"
)

var (
	notifiers = make([]chan struct{}, 0)
	holder    = validator{
		sum: "",
		mu:  sync.Mutex{},
	}
)

type validator struct {
	sum string
	mu  sync.Mutex
}

func (h *validator) updateSum(s string) {
	h.mu.Lock()
	h.sum = s
	h.mu.Unlock()
}

func RegisterNotifier(c chan struct{}) {
	notifiers = append(notifiers, c)
}

func InitConfigurationWatch(confFilePath string) {
	// 直接指定文件路径
	viper.SetConfigFile(confFilePath)
	viper.OnConfigChange(func(in fsnotify.Event) {
		slog.Info("检测到配置修改!", slog.Any("name", in.Name), slog.Any("op", in.Op))
	})
	viper.WatchConfig()
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func InitConfigurationEtcd(provider, endpoint, path string) {
	err := viper.AddRemoteProvider(provider,
		// 通过 webook 和其他使用 etcd 的区别出来
		endpoint, path)
	if err != nil {
		panic(err)
	}
	viper.SetConfigType("yaml")
	err = viper.ReadRemoteConfig()
	if err != nil {
		panic(err)
	}

	before, err := json.Marshal(viper.AllSettings())
	if err != nil {
		panic(err)
	}
	current := GetMD5Hash(before)
	holder.updateSum(current)

	go func() {
		for {
			time.Sleep(5 * time.Second)
			err = viper.WatchRemoteConfig()
			if err != nil {
				slog.Error("监听remote config错误", slog.Any("error", err))
				continue
			}
			after, err := json.Marshal(viper.AllSettings())
			if err != nil {
				slog.Error("解析配置错误", "error", err)
				continue
			}
			newSum := GetMD5Hash(after)
			if newSum == holder.sum {
				continue
			}
			slog.Info("检测到配置有所变更，开始发送通知")
			holder.updateSum(newSum)
			if len(notifiers) < 1 {
				continue
			}
			for _, notifier := range notifiers {
				notifier <- struct{}{}
			}
		}
	}()
}

func GetMD5Hash(bs []byte) string {
	hash := md5.Sum(bs)
	return hex.EncodeToString(hash[:])
}
