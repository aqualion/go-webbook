package conf

type WebookConfig struct {
	DB    DBConfig    `yaml:"db"`
	Redis RedisConfig `yaml:"redis"`
	App   AppConfig   `yaml:"app"`
}

type DBConfig struct {
	DSN string `yaml:"dsn"`
}

type RedisConfig struct {
	Addr     string `yaml:"addr"`
	DB       int    `yaml:"db"`
	Password string `yaml:"password"`
}

type AppConfig struct {
	Port string `yaml:"port"`
}

type RetryConfig struct {
	Type          string `yaml:"type"`
	IntervalMilli int64  `yaml:"intervalMilli"`
	Limit         int    `yaml:"limit"`
}

type OTELSetting struct {
	ServiceName       string `yaml:"serviceName"`
	ServiceVersion    string `yaml:"serviceVersion"`
	TracerProviderURL string `yaml:"tracerProviderUrl"`
	Enabled           bool   `yaml:"enabled"`
}
