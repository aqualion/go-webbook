## 第二周作业
edit接口截图
![第二周作业1](./doc/week2_1.png)

profile接口截图
![第二周作业2](./doc/week2_2.png)

db截图
![第二周作业3](./doc/week2_3.png)

## 第三周作业
get pods和get service截图
![第三周作业1](./doc/week3_1.png)

安装ingress controller后通过浏览器域名访问webook应用
![第三周作业1](./doc/week3_2.png)
