FROM ubuntu:20.04
COPY output/webook /app/webook
COPY config/dev-k8s.yaml /app/config.yaml
WORKDIR /app
ENTRYPOINT ["/app/webook", "--config", "config.yaml"]