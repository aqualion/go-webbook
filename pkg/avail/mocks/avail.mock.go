// Code generated by MockGen. DO NOT EDIT.
// Source: ./types.go
//
// Generated by this command:
//
//	mockgen -source=./types.go -package=availmocks -destination=./mocks/avail.mock.go
//
// Package availmocks is a generated GoMock package.
package availmocks

import (
	reflect "reflect"

	avail "gitee.com/aqualion/go-webbook/pkg/avail"
	gomock "go.uber.org/mock/gomock"
)

// MockAvailabilityChecker is a mock of AvailabilityChecker interface.
type MockAvailabilityChecker struct {
	ctrl     *gomock.Controller
	recorder *MockAvailabilityCheckerMockRecorder
}

// MockAvailabilityCheckerMockRecorder is the mock recorder for MockAvailabilityChecker.
type MockAvailabilityCheckerMockRecorder struct {
	mock *MockAvailabilityChecker
}

// NewMockAvailabilityChecker creates a new mock instance.
func NewMockAvailabilityChecker(ctrl *gomock.Controller) *MockAvailabilityChecker {
	mock := &MockAvailabilityChecker{ctrl: ctrl}
	mock.recorder = &MockAvailabilityCheckerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockAvailabilityChecker) EXPECT() *MockAvailabilityCheckerMockRecorder {
	return m.recorder
}

// CheckAvailability mocks base method.
func (m *MockAvailabilityChecker) CheckAvailability() (avail.AvailabilityResult, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CheckAvailability")
	ret0, _ := ret[0].(avail.AvailabilityResult)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CheckAvailability indicates an expected call of CheckAvailability.
func (mr *MockAvailabilityCheckerMockRecorder) CheckAvailability() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CheckAvailability", reflect.TypeOf((*MockAvailabilityChecker)(nil).CheckAvailability))
}
