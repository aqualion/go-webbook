package avail

// AvailabilityChecker 检查服务的可用性并返回可用性相关判断结果 AvailabilityResult
//
//go:generate mockgen -source=./types.go -package=availmocks -destination=./mocks/avail.mock.go
//go:generate mockgen -package=availmocks -destination=./mocks/cmdable.mock.go github.com/redis/go-redis/v9 Cmdable
type AvailabilityChecker interface {
	CheckAvailability() (AvailabilityResult, error)
}

type AvailabilityResult struct {
	Available bool
	Reason    string // 如果 Available为false, 返回原因
}
