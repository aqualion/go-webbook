package avail

import (
	"encoding/json"
	availmocks "gitee.com/aqualion/go-webbook/pkg/avail/mocks"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"testing"
)

func TestNewRedisMetricsChecker(t *testing.T) {
	testCases := []struct {
		name         string
		mockRedisCmd func(ctrl *gomock.Controller) redis.Cmdable
		// 输入
		conf RedisMetricsCheckerConf

		// 输出
		wantResult AvailabilityResult
		wantErr    error
	}{
		{
			name: "cpu占用率达到设定阈值, 服务不可用",
			mockRedisCmd: func(ctrl *gomock.Controller) redis.Cmdable {
				js, _ := json.Marshal(Metrics{
					CPUPercent: 50,
				})
				cmdable := availmocks.NewMockCmdable(ctrl)
				cmdable.EXPECT().Get(gomock.Any(), gomock.Any()).Return(redis.NewStringResult(string(js), nil))
				return cmdable
			},
			conf: RedisMetricsCheckerConf{
				TargetKey:        "test",
				ThresholdPercent: 50,
			},
			wantResult: AvailabilityResult{
				Available: false,
				Reason:    "目标服务cpu占用率:50%, 已超过设定阈值:50%",
			},
			wantErr: nil,
		},
		{
			name: "cpu占用率未达到设定阈值, 服务可用",
			mockRedisCmd: func(ctrl *gomock.Controller) redis.Cmdable {
				js, _ := json.Marshal(Metrics{
					CPUPercent: 49,
				})
				cmdable := availmocks.NewMockCmdable(ctrl)
				cmdable.EXPECT().Get(gomock.Any(), gomock.Any()).Return(redis.NewStringResult(string(js), nil))
				return cmdable
			},
			conf: RedisMetricsCheckerConf{
				TargetKey:        "test",
				ThresholdPercent: 50,
			},
			wantResult: AvailabilityResult{
				Available: true,
				Reason:    "",
			},
			wantErr: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			checker := NewRedisMetricsChecker(tc.mockRedisCmd(ctrl), tc.conf)
			result, err := checker.CheckAvailability()
			assert.ErrorIs(t, tc.wantErr, err)
			assert.Equal(t, tc.wantResult, result)
		})
	}
}
