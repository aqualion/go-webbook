package avail

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/redis/go-redis/v9"
)

type RedisMetricsChecker struct {
	client redis.Cmdable
	conf   RedisMetricsCheckerConf
}

func NewRedisMetricsChecker(client redis.Cmdable, conf RedisMetricsCheckerConf) AvailabilityChecker {
	return &RedisMetricsChecker{client: client, conf: conf}
}

type RedisMetricsCheckerConf struct {
	TargetKey        string // 通过这个key来获得cpu状态
	ThresholdPercent int    // cpu阈值%，多于这个值则判定为不可用
}

type Metrics struct {
	CPUPercent int `json:"cpuPercent"`
	MemPercent int `json:"memPercent"`
}

func (r *RedisMetricsChecker) CheckAvailability() (AvailabilityResult, error) {
	result, err := r.client.Get(context.Background(), r.conf.TargetKey).Result()
	if err != nil {
		return AvailabilityResult{}, err
	}
	metrics := Metrics{}
	err = json.Unmarshal([]byte(result), &metrics)
	if err != nil {
		return AvailabilityResult{}, err
	}
	thresholdPercent := r.conf.ThresholdPercent
	cpuPercent := metrics.CPUPercent
	if cpuPercent >= thresholdPercent {
		return AvailabilityResult{
			Available: false,
			Reason:    fmt.Sprintf("目标服务cpu占用率:%v%%, 已超过设定阈值:%v%%", cpuPercent, thresholdPercent),
		}, nil
	}
	return AvailabilityResult{Available: true}, nil
}
