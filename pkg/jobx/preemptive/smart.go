package preemptive

import (
	"gitee.com/aqualion/go-webbook/pkg/logger"
	"go.uber.org/atomic"
	"time"
)

// SmartScheduler 在分布式锁任务调度器基础上加入了负载判断功能
// 节点负载, 0~1000.
// 抢锁前根据负载值，计算一个延迟时间（毫秒级），负载低的节点先抢
// 定时检查自己的负载，如果一段时间内的负载一直达到阈值，并且持有锁，就释放锁
// Q: 分析极端情况。即有没有可能选中最差的节点？
// A: 概率很小，除非情况好的节点都抢锁失败了，或者情况好的节点在先手的情况下都抢不赢后手节点
// 如果选中的节点宕机了，会发生什么？
// 任务执行完成前就宕机：这次任务就执行失败了，可以发个告警。宕机后会因为没有续约，下次执行时锁会被其他节点占用
// 任务执行完后宕机：宕机后会因为没有续约，下次执行时锁会被其他节点占用
type SmartScheduler struct {
	name             string
	scheduler        Scheduler
	l                logger.LoggerV1
	remoteLockStatus *atomic.Bool
	load             *atomic.Float64
}

func NewSmartJob(name string, scheduler Scheduler, l logger.LoggerV1) *SmartScheduler {
	job := &SmartScheduler{
		name:             name,
		scheduler:        scheduler,
		load:             atomic.NewFloat64(0),
		remoteLockStatus: atomic.NewBool(false),
		l:                l,
	}
	go job.monitorLoad()
	return job
}

func (r *SmartScheduler) Run() (error, bool) {
	// 如果已经持有锁，无需抢锁，直接运行任务
	if r.remoteLockStatus.Load() {
		err, lockHeld := r.scheduler.Run()
		r.remoteLockStatus.Store(lockHeld)
		r.debug("锁持有状态", logger.Any("持有锁?", lockHeld), logger.Any("上次抢锁延迟", "已持有锁，无延迟"))
		return err, lockHeld
	}
	delay := r.calcDelayByLoad()
	time.Sleep(delay)
	err, lockHeld := r.scheduler.Run()
	r.remoteLockStatus.Store(lockHeld)
	r.debug("锁持有状态", logger.Any("持有锁?", lockHeld), logger.Any("上次抢锁延迟", delay))
	return err, lockHeld
}

func (r *SmartScheduler) Close() error {
	return r.scheduler.Close()
}

// UpdateLoad 更新负载
func (r *SmartScheduler) UpdateLoad(load float64) {
	r.load.Store(load)
}

// monitorLoad 定时检查自己的负载，如果一段时间内的负载一直达到阈值，并且持有锁，就释放锁
func (r *SmartScheduler) monitorLoad() {
	var tolerenceCount int
	for {
		time.Sleep(5 * time.Second)
		if r.load.Load() >= 600 {
			tolerenceCount++
		} else {
			tolerenceCount = 0
		}

		// 忍耐度还没到，再观察观察
		if tolerenceCount < 3 {
			continue
		}
		// 忍耐度到了
		// 而且这个节点持有锁，就释放锁
		// 如果有任务正在执行怎么办? 没关系，锁先放掉，任务继续执行，下次执行时锁就可能会被其他节点占用
		if r.remoteLockStatus.Load() {
			r.debug("超过忍耐度且持有锁，开始释放锁")
			if err := r.Close(); err != nil {
				r.l.Error("释放锁失败", logger.Error(err))
			}
			r.remoteLockStatus.Store(false)
			tolerenceCount = 0
			continue
		}
		// 忍耐度到了但没持有锁, 也有可能接下来负载会降低，所以还是重置忍耐度
		r.debug("超过忍耐度但并未持有锁，重置忍耐度")
		tolerenceCount = 0
	}
}

func (r *SmartScheduler) Name() string {
	return r.name
}

// calcDelayByLoad 抢锁前根据负载值，计算一个延迟时间（毫秒级），负载低的节点先抢
// 这里简单处理，负载是多少，就延迟多少毫秒
func (r *SmartScheduler) calcDelayByLoad() time.Duration {
	return time.Duration(r.load.Load()) * time.Millisecond
}

func (r *SmartScheduler) debug(msg string, args ...logger.Field) {
	args = append(args, logger.Any("scheduler_name", r.name))
	r.l.Debug(msg, args...)
}
