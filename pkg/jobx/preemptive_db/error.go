package preemptive_db

import "gorm.io/gorm"

var ErrNoMoreJob = gorm.ErrRecordNotFound
