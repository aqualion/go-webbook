package preemptive_db

import (
	"context"
	"gorm.io/gorm"
	"time"
)

type JobDAO interface {
	Preempt(ctx context.Context, interval time.Duration) (Job, error)
	UpdateNextTime(ctx context.Context, id int64, t time.Time) error
	UpdateUtime(ctx context.Context, id int64) error
	Release(ctx context.Context, id int64) error
	Insert(ctx context.Context, j Job) error
}

type GORMJobDAO struct {
	db *gorm.DB
}

func (dao *GORMJobDAO) Insert(ctx context.Context, j Job) error {
	now := time.Now().UnixMilli()
	j.Ctime = now
	j.Utime = now
	return dao.db.WithContext(ctx).Create(&j).Error
}

func NewGORMJobDAO(db *gorm.DB) JobDAO {
	return &GORMJobDAO{db: db}
}

func (dao *GORMJobDAO) Release(ctx context.Context, id int64) error {
	return dao.db.WithContext(ctx).Model(&Job{}).
		Where("id = ?", id).Updates(map[string]any{
		"status": jobStatusWaiting,
		"utime":  time.Now().UnixMilli(),
	}).Error
}

func (dao *GORMJobDAO) UpdateUtime(ctx context.Context, id int64) error {
	return dao.db.WithContext(ctx).Model(&Job{}).
		Where("id=?", id).Updates(map[string]any{
		"utime": time.Now().UnixMilli(),
	}).Error
}

func (dao *GORMJobDAO) Preempt(ctx context.Context, interval time.Duration) (Job, error) {
	db := dao.db.WithContext(ctx)
	for {
		// 每一个循环都重新计算 time.Now，因为之前可能已经花了一些时间了
		now := time.Now()
		var j Job

		// 到了调度的时间
		// 任务被调度有三个条件：
		// 1. 到了调度时间: next_time < now
		// 2. 没人调度: status = jobStatusWaiting
		// 3. 曾经有人调度，但是后面续约失败了，也就是这个调度的节点，可能已经崩溃了: status = jobStatusRunning AND utime < (now - refreshInterval)
		// utime < (now - refreshInterval)可翻译为：
		// 从当前时间向前滑动一个时间段(refreshInterval), 得到一个时间戳t
		// 如果 t 小于 utime, 证明refreshInterval前，有人续约过，我们不能抢占
		// t  utime  now

		// 如果 t 大于 utime, 证明refreshInterval前就已经没人续约了, 肯定是续约失败了, 我们可以抢占
		// utime  t  now

		// 举例：假设refresh interval是10秒
		// 例1: utime:第10秒，now: 第11秒, t=11-10=1, 那么 10(utime) > 1(t), 不抢占
		// 例2: utime:第10秒，now: 第21秒, t=21-10=11, 那么 10(utime) < 11(t), 可以抢占

		t := now.UnixMilli() - interval.Milliseconds()
		err := db.Where(
			"(next_time <= ? AND status = ?) or (utime < ? AND status = ?)",
			now.UnixMilli(), jobStatusWaiting, t, jobStatusRunning).
			First(&j).Error
		if err != nil {
			// 数据库有问题
			return Job{}, err
		}

		// 然后要开始抢占
		// 这里利用 utime 来执行 CAS 操作
		// 其它一些公司可能会有一些 version 之类的字段
		res := db.Model(&Job{}).
			Where("id = ? AND version=?", j.Id, j.Version).
			Updates(map[string]any{
				"utime":   now,
				"version": j.Version + 1,
				"status":  jobStatusRunning,
			})
		if res.Error != nil {
			// 数据库错误
			return Job{}, err
		}
		// 抢占成功
		if res.RowsAffected == 1 {
			return j, nil
		}
		// 没有抢占到，也就是同一时刻被人抢走了，那么就下一个循环
	}
}

func (dao *GORMJobDAO) UpdateNextTime(ctx context.Context, id int64, t time.Time) error {
	return dao.db.WithContext(ctx).Model(&Job{}).
		Where("id=?", id).Updates(map[string]any{
		"utime":     time.Now().UnixMilli(),
		"next_time": t.UnixMilli(),
	}).Error
}

type Job struct {
	Id         int64 `gorm:"primaryKey,autoIncrement"`
	Name       string
	Executor   string
	Cfg        string
	Expression string
	Version    int64
	NextTime   int64 `gorm:"index"`
	Status     int
	Ctime      int64
	Utime      int64
}

const (
	// 等待被调度，意思就是没有人正在调度
	jobStatusWaiting = iota
	// 已经被 goroutine 抢占了
	jobStatusRunning
	// 不再需要调度了，比如说被终止了，或者被删除了。
	// 我们这里没有严格区分这两种情况的必要性
	jobStatusEnd
)
