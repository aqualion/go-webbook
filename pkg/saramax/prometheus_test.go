package saramax

import (
	"context"
	"encoding/json"
	"errors"
	"gitee.com/aqualion/go-webbook/internal/ioc"
	"github.com/IBM/sarama"
	"github.com/google/uuid"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log/slog"
	"math/rand"
	"net/http"
	"testing"
	"time"
)

type TestMsg struct {
	Msg string `json:"msg"`
}

func TestMetrics(t *testing.T) {
	slog.Info("启动prometheus监听端口...")
	initPrometheus()
	slog.Info("启动kafka client...")
	client := initTestKafkaClient()

	slog.Info("启动kafka producer...")
	producer, err := sarama.NewSyncProducerFromClient(client)
	if err != nil {
		t.Fatal(err)
	}

	consumerGroup := "my_test_group"
	topic := "my_test_topic"

	cg, err := sarama.NewConsumerGroupFromClient(consumerGroup, client)
	if err != nil {
		t.Fatal(err)
	}

	rd := rand.New(rand.NewSource(time.Now().UnixMilli()))

	slog.Info("启动kafka consumer...")
	// 启动consumer
	go func() {
		logger := ioc.InitLogger()
		handlerFunc := func(msg *sarama.ConsumerMessage, t TestMsg) error {
			slog.Info("收到消息", "body", t)
			sleepPeriod := rd.Intn(3000)
			if sleepPeriod < 300 {
				return errors.New("假装失败")
			}
			time.Sleep(time.Duration(sleepPeriod) * time.Millisecond)
			return nil
		}
		opts := func(h *HandlerSetting) {
			h.Namespace = "my_test_namespace"
			h.Subsystem = "webook"
			h.InstanceID = "test_instance_id"
		}

		err := cg.Consume(context.Background(),
			[]string{topic},
			NewHandlerWithPrometheus[TestMsg](
				consumerGroup,
				logger,
				handlerFunc, opts,
			),
		)
		if err != nil {
			t.Fatal(err)
		}
	}()

	// 模拟producer随机发送消息
	for {
		randomUUID, err := uuid.NewRandom()
		if err != nil {
			t.Fatal(err)
		}

		var finalMsg string
		if rd.Intn(20) > 1 {
			jsonMsg, err := json.Marshal(TestMsg{Msg: randomUUID.String()})
			if err != nil {
				t.Fatal(err)
			}
			finalMsg = string(jsonMsg)
		} else {
			finalMsg = "故意发个错误的消息体"
		}
		_, _, err = producer.SendMessage(&sarama.ProducerMessage{
			Topic: topic,
			Key:   sarama.StringEncoder("my_key"),
			Value: sarama.StringEncoder(finalMsg),
		})
		if err != nil {
			t.Fatal(err)
		}

		time.Sleep(time.Duration(rd.Intn(500)) * time.Millisecond)
	}
}

func initPrometheus() {
	go func() {
		http.Handle("/metrics", promhttp.Handler())
		// 监听 8081 端口，你也可以做成可配置的
		slog.Info("prometheus listen on :18900")
		http.ListenAndServe(":18900", nil)
	}()
}

func initTestKafkaClient() sarama.Client {
	saramaCfg := sarama.NewConfig()
	saramaCfg.Producer.Return.Successes = true
	client, err := sarama.NewClient([]string{"localhost:9094"}, saramaCfg)
	if err != nil {
		panic(err)
	}
	return client
}
