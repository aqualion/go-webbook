package saramax

import (
	"encoding/json"
	"gitee.com/aqualion/go-webbook/pkg/logger"
	"github.com/IBM/sarama"
	"github.com/prometheus/client_golang/prometheus"
	"strconv"
	"time"
)

var (
	labels = []string{"topic", "partition", "group"}
)

type HandlerOption func(h *HandlerSetting)

type HandlerSetting struct {
	Namespace  string
	Subsystem  string
	InstanceID string
}

// HandlerWithPrometheus 在消费时提供一些监控
// 监控内容:
// Summary: topic + partition + cosumer gorup 的消息处理时间
// Gauge: topic + partition + cosumer gorup 的消息积压数(用high_watermark_offset - offset)
// Counter: topic + cosumer group + error type  监控某topic下某个消费组出现的不同错误种类的数量
type HandlerWithPrometheus[T any] struct {
	consumerGroup      string
	l                  logger.LoggerV1
	fn                 func(msg *sarama.ConsumerMessage, t T) error
	setting            *HandlerSetting
	elapsedTimeSummary *prometheus.SummaryVec // 消费耗时监控
	offsetDelayGauge   *prometheus.GaugeVec   // 消息积压监控
	errorCounter       *prometheus.CounterVec // 错误数监控
}

func NewHandlerWithPrometheus[T any](
	consumerGroup string,
	l logger.LoggerV1,
	fn func(msg *sarama.ConsumerMessage, t T) error,
	opts ...HandlerOption,
) *HandlerWithPrometheus[T] {
	handler := &HandlerWithPrometheus[T]{
		consumerGroup: consumerGroup,
		l:             l,
		fn:            fn,
		setting:       &HandlerSetting{},
	}
	for _, opt := range opts {
		opt(handler.setting)
	}

	handler.elapsedTimeSummary = newElapsedTimeSummaryVec(handler.setting)
	handler.offsetDelayGauge = newOffsetDelayGauge(handler.setting)
	handler.errorCounter = newErrorCounter(handler.setting)

	return handler
}

func newErrorCounter(setting *HandlerSetting) *prometheus.CounterVec {
	counter := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: setting.Namespace,
		Subsystem: setting.Subsystem,
		Name:      "consumer_error_count",
		Help:      "记录当前活跃的消费过程数量(主要是为了监控消息积压)",
		ConstLabels: map[string]string{
			"instance_id": setting.InstanceID,
		},
	}, []string{"topic", "group", "error_type"})
	prometheus.MustRegister(counter)
	return counter
}

func newOffsetDelayGauge(setting *HandlerSetting) *prometheus.GaugeVec {
	gauge := prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: setting.Namespace,
		Subsystem: setting.Subsystem,
		Name:      "consumer_offset_delay",
		Help:      "记录当前活跃的消费过程数量(主要是为了监控消息积压)",
		ConstLabels: map[string]string{
			"instance_id": setting.InstanceID,
		},
	}, labels)
	prometheus.MustRegister(gauge)
	return gauge
}

func newElapsedTimeSummaryVec(setting *HandlerSetting) *prometheus.SummaryVec {
	vec := prometheus.NewSummaryVec(prometheus.SummaryOpts{
		Namespace:   setting.Namespace,
		Subsystem:   setting.Subsystem,
		Name:        "consumer_elapsed_time_ms",
		Help:        "记录消费耗时",
		ConstLabels: map[string]string{"instance_id": setting.InstanceID},
		Objectives: map[float64]float64{
			0.9:  0.01,
			0.99: 0.001,
		},
	}, labels)
	prometheus.MustRegister(vec)
	return vec
}

// recordElapsedTime 记录消费耗时
func (h *HandlerWithPrometheus[T]) recordElapsedTime(topic string, partition int32, start time.Time) {
	h.elapsedTimeSummary.WithLabelValues(topic, strconv.Itoa(int(partition)), h.consumerGroup).
		Observe(float64(time.Since(start).Milliseconds()))
}

// recordOffsetDelay 记录消息积压
func (h *HandlerWithPrometheus[T]) recordOffsetDelay(topic string, partition int32, delta int64) {
	h.offsetDelayGauge.WithLabelValues(topic, strconv.Itoa(int(partition)), h.consumerGroup).Set(float64(delta))
}

// recordError 记录一次错误
func (h *HandlerWithPrometheus[T]) recordError(topic string, partition int32, errType string) {
	h.errorCounter.WithLabelValues(topic, h.consumerGroup, errType).Inc()
}

func (h *HandlerWithPrometheus[T]) Setup(session sarama.ConsumerGroupSession) error {
	return nil
}

func (h *HandlerWithPrometheus[T]) Cleanup(session sarama.ConsumerGroupSession) error {
	return nil
}

// ConsumeClaim 可以考虑在这个封装里面提供统一的重试机制
func (h *HandlerWithPrometheus[T]) ConsumeClaim(session sarama.ConsumerGroupSession,
	claim sarama.ConsumerGroupClaim) error {
	msgs := claim.Messages()

	for msg := range msgs {
		highWaterMarkOffset := claim.HighWaterMarkOffset()
		offset := msg.Offset
		topic := msg.Topic
		partition := msg.Partition
		startTime := time.Now()

		h.recordOffsetDelay(topic, partition, highWaterMarkOffset-offset)

		var t T
		err := json.Unmarshal(msg.Value, &t)
		if err != nil {
			// 消息格式都不对，没啥好处理的
			// 但是也不能直接返回，在线上的时候要继续处理下去
			h.l.Error("反序列化消息体失败",
				logger.Any("topic", topic),
				logger.Any("partition", partition),
				logger.Int64("offset", offset),
				// 这里也可以考虑打印 Msg.Value，但是有些时候 Msg 本身也包含敏感数据
				logger.Error(err))
			// 不中断，继续下一个
			session.MarkMessage(msg, "")

			h.recordError(topic, partition, "消息反序列化失败")
			h.recordElapsedTime(topic, partition, startTime)
			continue
		}
		err = h.fn(msg, t)
		if err != nil {
			h.l.Error("处理消息失败",
				logger.Any("topic", topic),
				logger.Any("partition", partition),
				logger.Int64("offset", offset),
				logger.Error(err))
			h.recordError(topic, partition, "业务处理失败")
		}
		session.MarkMessage(msg, "")
		h.recordElapsedTime(topic, partition, startTime)
	}
	return nil
}
