package saramax

import (
	"encoding/json"
	"gitee.com/aqualion/go-webbook/pkg/logger"
	"github.com/IBM/sarama"
)

type Handler[T any] struct {
	l  logger.LoggerV1
	fn func(msg *sarama.ConsumerMessage, t T) error
}

func NewHandler[T any](l logger.LoggerV1,
	fn func(msg *sarama.ConsumerMessage, t T) error) *Handler[T] {
	return &Handler[T]{
		l:  l,
		fn: fn,
	}
}

func (h *Handler[T]) Setup(session sarama.ConsumerGroupSession) error {
	return nil
}

func (h *Handler[T]) Cleanup(session sarama.ConsumerGroupSession) error {
	return nil
}

// ConsumeClaim 可以考虑在这个封装里面提供统一的重试机制
func (h *Handler[T]) ConsumeClaim(session sarama.ConsumerGroupSession,
	claim sarama.ConsumerGroupClaim) error {
	msgs := claim.Messages()
	for msg := range msgs {
		// TODO 添加prometheus监控
		// 监控内容:
		// topic + partition 的消息处理时间
		var t T
		err := json.Unmarshal(msg.Value, &t)
		if err != nil {
			// 消息格式都不对，没啥好处理的
			// 但是也不能直接返回，在线上的时候要继续处理下去
			h.l.Error("反序列化消息体失败",
				logger.Any("topic", msg.Topic),
				logger.Any("partition", msg.Partition),
				logger.Int64("offset", msg.Offset),
				// 这里也可以考虑打印 Msg.Value，但是有些时候 Msg 本身也包含敏感数据
				logger.Error(err))
			// 不中断，继续下一个
			session.MarkMessage(msg, "")
			continue
		}
		err = h.fn(msg, t)
		if err != nil {
			h.l.Error("处理消息失败",
				logger.Any("topic", msg.Topic),
				logger.Any("partition", msg.Partition),
				logger.Int64("offset", msg.Offset),
				logger.Error(err))
		}
		session.MarkMessage(msg, "")
	}
	return nil
}
