package otelx

import (
	"context"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/zipkin"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.21.0"
	"time"
)

// InitOTEL 返回一个关闭函数，并且让调用者关闭的时候来决定这个 ctx
func InitOTEL(opts ...OTELOption) (func(ctx context.Context), chan struct{}) {
	// 默认值
	setting := &OTELSetting{
		ServiceName:    "my_service",
		ServiceVersion: "v0.0.1",
		Enabled:        false,
		SettingFetcher: nil,
	}
	for _, opt := range opts {
		opt(setting)
	}

	setting.validate()

	res, err := newResource(setting.ServiceName, setting.ServiceVersion)
	if err != nil {
		panic(err)
	}
	prop := newPropagator()
	// 在客户端和服务端之间传递 tracing 的相关信息
	otel.SetTextMapPropagator(prop)

	// 初始化 trace provider
	// 这个 provider 就是用来在打点的时候构建 trace 的
	tp, err := newTraceProvider(res, setting.TracerProviderURL, setting.Enabled, setting.SettingFetcher)
	if err != nil {
		panic(err)
	}

	otel.SetTracerProvider(tp)
	return func(ctx context.Context) {
		_ = tp.Shutdown(ctx)
	}, tp.GetNotifierChan()
}

func newResource(serviceName, serviceVersion string) (*resource.Resource, error) {
	return resource.Merge(resource.Default(),
		resource.NewWithAttributes(semconv.SchemaURL,
			semconv.ServiceName(serviceName),
			semconv.ServiceVersion(serviceVersion),
		))
}

func newTraceProvider(
	res *resource.Resource,
	providerUrl string,
	enabled bool,
	settingFetcher func() OTELSetting,
) (TracerProviderX, error) {
	exporter, err := zipkin.New(providerUrl)

	if err != nil {
		return nil, err
	}

	traceProvider := trace.NewTracerProvider(
		trace.WithBatcher(exporter,
			// Default is 5s. Set to 1s for demonstrative purposes.
			trace.WithBatchTimeout(time.Second)),
		trace.WithResource(res),
	)

	tp := NewTracerProviderX(traceProvider, enabled, settingFetcher)
	return tp, nil
}

func newPropagator() propagation.TextMapPropagator {
	return propagation.NewCompositeTextMapPropagator(
		propagation.TraceContext{},
		propagation.Baggage{},
	)
}
