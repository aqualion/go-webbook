package otelx

import (
	"context"
	traceSdk "go.opentelemetry.io/otel/sdk/trace"
	"go.opentelemetry.io/otel/trace"
	"go.opentelemetry.io/otel/trace/embedded"
	"go.opentelemetry.io/otel/trace/noop"
	"log/slog"
	"sync"
)

type TracerProviderX interface {
	trace.TracerProvider
	Shutdown(ctx context.Context) error
	GetNotifierChan() chan struct{}
}

type TracerProviderXImpl struct {
	embedded.TracerProvider
	enabled        bool
	noop           trace.TracerProvider
	tp             trace.TracerProvider
	mu             sync.Mutex
	notifier       chan struct{}
	settingFetcher func() OTELSetting
}

func NewTracerProviderX(tp trace.TracerProvider, enabled bool, settingFetcher func() OTELSetting) *TracerProviderXImpl {
	t := &TracerProviderXImpl{
		TracerProvider: tp,
		enabled:        enabled,
		tp:             tp,
		noop:           noop.NewTracerProvider(),
		notifier:       make(chan struct{}),
		settingFetcher: settingFetcher,
	}
	t.WatchConfChanges()
	return t
}

func (t *TracerProviderXImpl) GetNotifierChan() chan struct{} {
	return t.notifier
}

func (t *TracerProviderXImpl) setEnabled(enabled bool) {
	t.mu.Lock()
	t.enabled = enabled
	t.mu.Unlock()
}

func (t *TracerProviderXImpl) Shutdown(ctx context.Context) error {
	if t.enabled {
		p, ok := t.tp.(*traceSdk.TracerProvider)
		if !ok {
			return nil
		}
		return p.Shutdown(ctx)
	}
	return nil
}

func (t *TracerProviderXImpl) Tracer(name string, options ...trace.TracerOption) trace.Tracer {
	if t.enabled {
		return t.tp.Tracer(name, options...)
	}
	return t.noop.Tracer(name, options...)
}

func (t *TracerProviderXImpl) WatchConfChanges() {
	go func() {
		for range t.notifier {
			slog.Info("TracerProviderXImpl: 检测到配置文件发生变更!")
			newSetting := t.settingFetcher()
			t.setEnabled(newSetting.Enabled)
		}
	}()

}
