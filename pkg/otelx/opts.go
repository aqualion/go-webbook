package otelx

type OTELSetting struct {
	ServiceName       string
	ServiceVersion    string
	TracerProviderURL string
	Enabled           bool
	SettingFetcher    func() OTELSetting
}

func (setting *OTELSetting) validate() {
	if setting.TracerProviderURL == "" {
		panic("err init otel: tracer-provider-url is required!")
	}
	if setting.ServiceName == "" {
		panic("err init otel: service-name is required!")
	}
	if setting.ServiceVersion == "" {
		panic("err init otel: service-version is required!")
	}
}

type OTELOption func(setting *OTELSetting)

func WithServiceName(serviceName string) OTELOption {
	return func(setting *OTELSetting) {
		if serviceName == "" {
			return
		}
		setting.ServiceName = serviceName
	}
}

func WithServiceVersion(serviceVersion string) OTELOption {
	return func(setting *OTELSetting) {
		if serviceVersion == "" {
			return
		}
		setting.ServiceVersion = serviceVersion
	}
}

func WithTracerProviderURL(tracerProviderURL string) OTELOption {
	return func(setting *OTELSetting) {
		if tracerProviderURL == "" {
			return
		}
		setting.TracerProviderURL = tracerProviderURL
	}
}

func WithEnabled(enabled bool) OTELOption {
	return func(setting *OTELSetting) {
		setting.Enabled = enabled
	}
}

func WithSettingFetcher(f func() OTELSetting) OTELOption {
	return func(setting *OTELSetting) {
		setting.SettingFetcher = f
	}
}
