package rlock

import (
	"context"
	"errors"
	cachemocks "gitee.com/aqualion/go-webbook/pkg/rlock/mocks"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"testing"
	"time"
)

func TestClient_TryLock(t *testing.T) {
	type args struct {
		ctx        context.Context
		key        string
		expiration time.Duration
		redisMock  func(ctrl *gomock.Controller) redis.Cmdable
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		{
			name: "设置成功",
			args: args{
				ctx:        context.Background(),
				key:        "some_biz",
				expiration: 10 * time.Second,
				redisMock: func(ctrl *gomock.Controller) redis.Cmdable {
					m := cachemocks.NewMockCmdable(ctrl)
					res := redis.NewBoolCmd(context.Background())
					res.SetVal(true)
					m.EXPECT().SetNX(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
						Return(res)
					return m
				},
			},
			wantErr: nil,
		},
		{
			name: "redis调用发生错误",
			args: args{
				ctx:        context.Background(),
				key:        "some_biz",
				expiration: 10 * time.Second,
				redisMock: func(ctrl *gomock.Controller) redis.Cmdable {
					m := cachemocks.NewMockCmdable(ctrl)
					res := redis.NewBoolCmd(context.Background())
					err := errors.New("some_redis_err")
					res.SetErr(err)
					m.EXPECT().SetNX(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
						Return(res)
					return m
				},
			},
			wantErr: ErrCallingRedis,
		},
		{
			name: "调用redis成功但加锁失败",
			args: args{
				ctx:        context.Background(),
				key:        "some_biz",
				expiration: 10 * time.Second,
				redisMock: func(ctrl *gomock.Controller) redis.Cmdable {
					m := cachemocks.NewMockCmdable(ctrl)
					res := redis.NewBoolCmd(context.Background())
					res.SetVal(false)
					m.EXPECT().SetNX(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
						Return(res)
					return m
				},
			},
			wantErr: ErrLockFailed,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			c := NewClient(tt.args.redisMock(ctrl))

			got, err := c.TryLock(tt.args.ctx, tt.args.key, tt.args.expiration)

			assert.ErrorIs(t, err, tt.wantErr)
			if err != nil {
				return
			}

			gotKey := got.key
			gotValue := got.value

			assert.Equal(t, tt.args.key, gotKey)
			assert.NotEmpty(t, gotValue)
		})
	}
}

func TestLock_Unlock(t *testing.T) {
	type fields struct {
		redisMock func(ctrl *gomock.Controller) redis.Cmdable
		key       string
		value     string
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr error
	}{
		{
			name: "解锁成功",
			fields: fields{
				redisMock: func(ctrl *gomock.Controller) redis.Cmdable {
					m := cachemocks.NewMockCmdable(ctrl)
					cmd := redis.NewCmd(context.Background())
					cmd.SetErr(nil)
					cmd.SetVal(int64(1))
					m.EXPECT().Eval(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
						Return(cmd)
					return m
				},
				key:   "some_biz",
				value: "123",
			},
			args: args{
				ctx: context.Background(),
			},
			wantErr: nil,
		},
		{
			name: "redis key不存在",
			fields: fields{
				redisMock: func(ctrl *gomock.Controller) redis.Cmdable {
					m := cachemocks.NewMockCmdable(ctrl)
					cmd := redis.NewCmd(context.Background())
					cmd.SetErr(redis.Nil)
					m.EXPECT().Eval(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
						Return(cmd)
					return m
				},
				key:   "some_biz",
				value: "123",
			},
			args: args{
				ctx: context.Background(),
			},
			wantErr: ErrLockNotHeld,
		},
		{
			name: "调用redis错误",
			fields: fields{
				redisMock: func(ctrl *gomock.Controller) redis.Cmdable {
					m := cachemocks.NewMockCmdable(ctrl)
					cmd := redis.NewCmd(context.Background())
					cmd.SetErr(errors.New("some_redis_err"))
					m.EXPECT().Eval(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
						Return(cmd)
					return m
				},
				key:   "some_biz",
				value: "123",
			},
			args: args{
				ctx: context.Background(),
			},
			wantErr: ErrCallingRedis,
		},
		{
			name: "锁不是自己加的，或者key不存在",
			fields: fields{
				redisMock: func(ctrl *gomock.Controller) redis.Cmdable {
					m := cachemocks.NewMockCmdable(ctrl)
					cmd := redis.NewCmd(context.Background())
					cmd.SetErr(nil)
					cmd.SetVal(int64(0))
					m.EXPECT().Eval(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
						Return(cmd)
					return m
				},
				key:   "some_biz",
				value: "123",
			},
			args: args{
				ctx: context.Background(),
			},
			wantErr: ErrLockNotHeld,
		},
	}
	for _, tt := range tests {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		t.Run(tt.name, func(t *testing.T) {
			c := &Lock{
				client: tt.fields.redisMock(ctrl),
				key:    tt.fields.key,
				value:  tt.fields.value,
			}
			err := c.Unlock(tt.args.ctx)
			assert.ErrorIs(t, err, tt.wantErr)
		})
	}
}
