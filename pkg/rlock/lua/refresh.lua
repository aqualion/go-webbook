if redis.call("GET", KEYS[1]) == ARGV[1] then
    return redis.call("PEXPIRE", KEYS[1], ARGV[2])
else
-- 走到这里，说明key不存在，或者key对应的值与传入的值不匹配
    return 0
end