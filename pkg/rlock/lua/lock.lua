local key = KEYS[1]
local value = ARGV[1]
local expiration = ARGV[2]

if redis.call("GET", key) == value then
-- 加锁已经成功了, 这里就刷新过期时间
    return redis.call("PEXPIRE", key, expiration)
else
-- 加锁, nx: 不存在时才set, 如果存在说明别人已经加过锁了; px: 指定超时时间单位为毫秒
    return redis.call("SET", key, value, "NX", "PX", expiration)
end