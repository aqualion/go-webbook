package rlock

import (
	"context"
	_ "embed"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/redis/go-redis/v9"
	"golang.org/x/sync/singleflight"
	"time"
)

var (
	ErrLockNotHeld  = errors.New("锁未被持有")
	ErrLockFailed   = errors.New("加锁失败")
	ErrCallingRedis = errors.New("redis调用错误")

	//go:embed lua/unlock.lua
	luaUnlock string
	//go:embed lua/refresh.lua
	luaRefresh string
	//go:embed lua/lock.lua
	luaLock string
)

//go:generate mockgen -package=cachemocks -destination=./mocks/cmdable.mock.go github.com/redis/go-redis/v9 Cmdable
type Client struct {
	client redis.Cmdable
	sfg    singleflight.Group
}

func NewClient(c redis.Cmdable) *Client {
	return &Client{c, singleflight.Group{}}
}

// SingleFlightLock 加入singlelight保护机制的加锁
func (c *Client) SingleFlightLock(
	ctx context.Context,
	key string,
	expiration time.Duration,
	retryStrat RetryStragegy,
	timeout time.Duration,
) (*Lock, error) {
	for {
		flag := false
		resCh := c.sfg.DoChan(key, func() (any, error) {
			flag = true
			return c.Lock(ctx, key, expiration, retryStrat, timeout)
		})

		select {
		case res := <-resCh:
			if flag {
				return res.Val.(*Lock), res.Err
			}
		case <-ctx.Done():
			return nil, ctx.Err()
		}
	}
}

// Lock 带重试策略的加锁
func (c *Client) Lock(
	ctx context.Context,
	key string,
	expiration time.Duration,
	retryStrat RetryStragegy,
	timeout time.Duration,
) (*Lock, error) {
	value := uuid.New().String()
	var timer *time.Timer
	defer func() {
		if timer != nil {
			timer.Stop()
		}
	}()

	for {
		lctx, cancel := context.WithTimeout(ctx, timeout)
		lockSuccess, err := c.client.Eval(lctx, luaLock, []string{key}, value, expiration.Seconds()).Bool()
		cancel()
		if err != nil && !errors.Is(err, context.DeadlineExceeded) {
			// 发现不是超时错误，无需重试
			return nil, err
		}
		// 加锁成功
		if lockSuccess {
			return newLock(c.client, key, value, expiration), nil
		}
		// 因为超时错误而导致加锁失败，执行重试
		interval, ok := retryStrat.Next()
		if !ok {
			// 最大重试次数到了，停止重试
			return nil, err
		}
		if timer == nil {
			timer = time.NewTimer(interval)
		}
		timer.Reset(interval)
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		case <-timer.C:
		}
	}
}

// TryLock 尝试加锁
func (c *Client) TryLock(ctx context.Context, key string, expiration time.Duration) (*Lock, error) {
	value := uuid.New().String()
	setSuccess, err := c.client.SetNX(ctx, key, value, expiration).Result()
	if err != nil {
		return nil, errors.Wrap(ErrCallingRedis, err.Error())
	}
	if !setSuccess {
		return nil, ErrLockFailed
	}
	return newLock(c.client, key, value, expiration), nil
}

type Lock struct {
	client     redis.Cmdable
	key        string
	value      string
	expiration time.Duration
	unlockCh   chan struct{}
}

func newLock(c redis.Cmdable, key, value string, expiration time.Duration) *Lock {
	return &Lock{c, key, value, expiration, make(chan struct{}, 1)}
}

// Unlock 解锁
// 通过value确认需要解锁的锁确实是自己加的锁
func (l *Lock) Unlock(ctx context.Context) error {
	defer func() {
		// 通知自动续约退出
		l.unlockCh <- struct{}{}
		close(l.unlockCh)
	}()
	// 这里需要先检查key对应的value必须是加锁时自己设置的value，否则不允许执行删除操作
	deleted, err := l.client.Eval(ctx, luaUnlock, []string{l.key}, l.value).Int64()
	if errors.Is(err, redis.Nil) {
		return ErrLockNotHeld
	}
	if err != nil {
		return errors.Wrap(ErrCallingRedis, err.Error())
	}
	if deleted == 0 {
		// 说明锁不是自己加的, 或者key不存在, 不允许删除
		return ErrLockNotHeld
	}

	return nil
}

// Refresh 手动续约
func (l *Lock) Refresh(ctx context.Context) error {
	// 这里需要先检查key对应的value必须是加锁时自己设置的value，否则不允许执行续约操作
	res, err := l.client.Eval(ctx,
		luaRefresh,
		[]string{l.key},
		l.value, l.expiration.Milliseconds()).Int64()

	if errors.Is(err, redis.Nil) {
		return ErrLockNotHeld
	}
	if err != nil {
		return errors.Wrap(ErrCallingRedis, err.Error())
	}
	if res != 1 {
		// 说明锁不是自己加的, 或者key不存在, 不允许续约
		return ErrLockNotHeld
	}

	return nil
}

// AutoRefresh 自动续约
func (l *Lock) AutoRefresh(interval time.Duration, timeout time.Duration) error {
	ticker := time.NewTicker(interval)
	retryCh := make(chan struct{}, 1)
	defer close(retryCh)
	for {
		select {
		// 解锁后退出续约
		case <-l.unlockCh:
			return nil
		// 重试续约
		case <-retryCh:
			ctx, cancel := context.WithTimeout(context.Background(), timeout)
			err := l.Refresh(ctx)
			cancel()
			if errors.Is(err, context.DeadlineExceeded) {
				// 立刻重试
				retryCh <- struct{}{}
				continue
			}
			if err != nil {
				return err
			}
			// 正常续约
		case <-ticker.C:
			ctx, cancel := context.WithTimeout(context.Background(), timeout)
			err := l.Refresh(ctx)
			cancel()
			if errors.Is(err, context.DeadlineExceeded) {
				// 立刻重试
				retryCh <- struct{}{}
				continue
			}
			if err != nil {
				return err
			}
		}
	}
}
