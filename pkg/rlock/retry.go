package rlock

import "time"

type RetryStragegy interface {
	// Next 返回下一次重试的间隔；如果不需要继续重试，ok返回false
	Next() (time.Duration, bool)
}

type FixedIntervalRetry struct {
	Interval time.Duration
	Max      int
	cnt      int
}

// Next 返回下一次重试的间隔；如果不需要继续重试，ok返回false
func (f *FixedIntervalRetry) Next() (time.Duration, bool) {
	f.cnt++
	return f.Interval, f.cnt <= f.Max
}
