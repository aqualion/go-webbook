package integration

import "gitee.com/aqualion/go-webbook/pkg/migrator"

type Interactive struct {
	Id         int64  `gorm:"primaryKey,autoIncrement"`
	BizId      int64  `gorm:"uniqueIndex:biz_type_id"`
	Biz        string `gorm:"type:varchar(128);uniqueIndex:biz_type_id"`
	ReadCnt    int64
	CollectCnt int64
	LikeCnt    int64
	Ctime      int64
	Utime      int64
}

func (i Interactive) ID() int64 {
	return i.Id
}

func (i Interactive) TableName() string {
	return "interactives"
}

func (i Interactive) CompareTo(entity migrator.Entity) bool {
	dst := entity.(migrator.Entity)
	return i == dst
}
