package startup

import (
	"gitee.com/aqualion/go-webbook/pkg/logger"
	"go.uber.org/zap"
)

func InitLog() logger.LoggerV1 {
	config := zap.NewDevelopmentConfig()
	l, err := config.Build()
	if err != nil {
		panic(err)
	}

	return logger.NewZapLogger(l)
}
