package integration

import (
	"context"
	"gitee.com/aqualion/go-webbook/pkg/migrator/events"
	"gitee.com/aqualion/go-webbook/pkg/migrator/events/fixer"
	"gitee.com/aqualion/go-webbook/pkg/migrator/integration/startup"
	"gitee.com/aqualion/go-webbook/pkg/migrator/validator"
	"github.com/IBM/sarama"
	"testing"
)

var topic string = "DATA_FIX"

// TestValidateAndFix 展示validator和fixer的用法
func TestValidateAndFix(t *testing.T) {
	srcDB := startup.InitSrcDB()
	dstDB := startup.InitIntrDB()
	kafkaClient := startup.InitKafka()
	syncProducer, err := sarama.NewSyncProducerFromClient(kafkaClient)
	if err != nil {
		panic(err)
	}
	logger := startup.InitLog()

	// 启动修复器
	dataFixer, err := fixer.NewConsumer[Interactive](
		kafkaClient,
		logger,
		topic,
		srcDB,
		dstDB,
	)

	if err := dataFixer.Start(); err != nil {
		panic(err)
	}

	// 启动校验器
	vali := validator.NewValidator[Interactive](
		srcDB,
		dstDB,
		"SRC",
		logger,
		events.NewSaramaProducer(syncProducer, topic),
	)
	if err := vali.Validate(context.Background()); err != nil {
		panic(err)
	}
}
