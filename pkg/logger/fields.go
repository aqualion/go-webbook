package logger

func Error(err error) Field {
	return Field{
		Key:   "error",
		Value: err,
	}
}

func Int64(key string, val int64) Field {
	return Field{
		Key:   key,
		Value: val,
	}
}

func Bool(key string, b bool) Field {
	return Field{
		Key:   key,
		Value: b,
	}

}

func Any(key string, val any) Field {
	return Field{
		Key:   key,
		Value: val,
	}
}

func String(key string, val string) Field {
	return Field{
		Key:   key,
		Value: val,
	}
}
