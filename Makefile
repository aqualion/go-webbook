.PHONY: build-app
build-app:
	@go build -o ./output/webook-app ./cmd/server/.

.PHONY: docker
docker:
	# 执行go mod tidy
	@go mod tidy
	# 执行go build
	@GOOS=linux GOARCH=arm go build -tags=k8s -o ./output/webook ./cmd/server/.
	# 执行 docker build
	@docker build -t aqualion/webook:v0.0.1 .
	# 部署app到k8s
	@kubectl apply -f k8s-webook-app.yaml
	@kubectl apply -f k8s-webook-ingress.yaml
	@kubectl apply -f k8s-webook-mysql.yaml
	@kubectl apply -f k8s-webook-redis.yaml
	# 重启deployment
	@kubectl rollout restart deployment webook


.PHONY: mock
mock:
	@go mod tidy
	@go generate ./...

.PHONY: wire
wireMain:
	# 生成server依赖注入
	@wire ./cmd/server/wire.go


.PHONY: wireIntegration
wireIntegration:
	# 生成集成测试依赖注入
	@wire ./internal/integration/startup/.
